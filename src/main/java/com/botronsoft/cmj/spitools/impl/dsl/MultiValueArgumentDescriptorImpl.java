package com.botronsoft.cmj.spitools.impl.dsl;

import com.botronsoft.cmj.spitools.dsl.ArgumentContainerDescriptor;
import com.botronsoft.cmj.spitools.dsl.MultiValueArgumentDescriptor;

/**
 * Class for describing list of single values argument.
 */
public class MultiValueArgumentDescriptorImpl extends AbstractArgumentDescriptor<MultiValueArgumentDescriptor>
		implements MultiValueArgumentDescriptor {

	private String separator;

	public MultiValueArgumentDescriptorImpl(String argumentName, ArgumentContainerDescriptor container) {
		super(argumentName, container);
	}

	@Override
	public MultiValueArgumentDescriptor withSeparator(String separator) {
		this.separator = separator;
		return this;
	}

	public String getSeparator() {
		return separator;
	}
}
