package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.dsl.ArgumentType;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class WorkflowArgumentTransformer extends AbstractArgumentTransformer<JiraWorkflow> implements ArgumentTransformer{

    public WorkflowArgumentTransformer(JiraManagersService jiraManagersService) {
        super(jiraManagersService);
    }

    @Override
    protected JiraWorkflow findReferenceById(String value) {
        throw new IllegalArgumentException(getUnsupportedReferenceTypeMessage());
    }

    @Override
    protected JiraWorkflow findReferenceByName(String value) {
        WorkflowManager workflowManager = jiraManagersService.getWorkflowManager();
        List<JiraWorkflow> workflows = workflowManager.getWorkflows().stream().filter(wf -> wf.getName().equalsIgnoreCase(value)).collect(Collectors.toList());
        return findFirstMatch(workflows, value, "workflow");
    }

    @Override
    protected void collectReference(String key, JiraWorkflow workflow, ConfigurationReferenceCollector referenceCollector) {
        referenceCollector.collectJiraWorkflow(key, workflow);
    }

    @Override
    protected String getReferenceId(JiraWorkflow reference) {
        throw new IllegalArgumentException(getUnsupportedReferenceTypeMessage());
    }

    @Override
    protected String getReferenceName(JiraWorkflow workflow) {
        return workflow.getName();
    }

    @Override
    protected Optional<JiraWorkflow> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
        return referenceLookup.lookupJiraWorkflow(key);
    }

    private String getUnsupportedReferenceTypeMessage() {
        return "Unsupported reference type '" + ReferenceType.BY_ID + "' for argument of type '" + ArgumentType.WORKFLOW + "'.";
    }
}
