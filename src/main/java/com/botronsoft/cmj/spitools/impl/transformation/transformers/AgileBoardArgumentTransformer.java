package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AgileBoardArgumentTransformer extends AbstractArgumentTransformer<Long> implements ArgumentTransformer{

    public AgileBoardArgumentTransformer(JiraManagersService jiraManagersService) {
        super(jiraManagersService);
    }

    @Override
    protected Long findReferenceById(String value) {
        return Long.valueOf(value);
    }

    @Override
    protected Long findReferenceByName(String value) {
        throw new UnsupportedOperationException("Agile Boards can only be referred by id.");
    }

    @Override
    protected void collectReference(String key, Long reference, ConfigurationReferenceCollector referenceCollector) {
        referenceCollector.collectAgileBoard(key, reference);
    }

    @Override
    protected String getReferenceId(Long reference) {
        return String.valueOf(reference);
    }

    @Override
    protected String getReferenceName(Long reference) {
        throw new UnsupportedOperationException("Agile Boards can only be referred by id.");
    }

    @Override
    protected Optional<Long> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
        return referenceLookup.lookupAgileBoard(key);
    }

}
