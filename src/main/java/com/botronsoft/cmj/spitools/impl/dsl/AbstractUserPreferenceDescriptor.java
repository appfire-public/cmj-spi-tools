package com.botronsoft.cmj.spitools.impl.dsl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.botronsoft.cmj.spitools.dsl.UserPreferenceContainerDescriptor;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceType;

public class AbstractUserPreferenceDescriptor<T extends UserPreferenceDescriptor<?>> implements UserPreferenceDescriptor<T> {

	private final String userPreferenceName;
	private final UserPreferenceContainerDescriptor container;

	private UserPreferenceType userPreferenceType = null;
	private ReferenceValueType referenceValueType = ReferenceValueType.ID;
	private String referenceValuePrefix = "";
	private final Set<String> literalValues = new HashSet<>();

	public AbstractUserPreferenceDescriptor(String userPreferenceName, UserPreferenceContainerDescriptor container) {
		this.userPreferenceName = userPreferenceName;
		this.container = container;
	}

	@Override
	public UserPreferenceContainerDescriptor and() {
		return container;
	}

	@Override
	public T ofType(UserPreferenceType userPreferenceType) {
		this.userPreferenceType = userPreferenceType;
		return self();
	}

	@Override
	public T byId() {
		referenceValueType = ReferenceValueType.ID;
		return self();
	}

	@Override
	public T byName() {
		referenceValueType = ReferenceValueType.NAME;
		return self();
	}

	@Override
	public T byKey() {
		referenceValueType = ReferenceValueType.KEY;
		return self();
	}

	@Override
	public T withPrefix(String prefix) {
		referenceValuePrefix = prefix;
		return self();
	}

	@Override
	public T withLiteralValues(String... literalValues) {
		Collections.addAll(this.literalValues, literalValues);
		return self();
	}

	public String getUserPreferenceName() {
		return userPreferenceName;
	}

	public UserPreferenceType getUserPreferenceType() {
		return userPreferenceType;
	}

	public ReferenceValueType getReferenceValueType() {
		return referenceValueType;
	}

	public String getReferenceValuePrefix() {
		return referenceValuePrefix;
	}

	public Set<String> getLiteralValues() {
		return literalValues;
	}

	@SuppressWarnings("unchecked")
	private T self() {
		return (T) this;
	}
}
