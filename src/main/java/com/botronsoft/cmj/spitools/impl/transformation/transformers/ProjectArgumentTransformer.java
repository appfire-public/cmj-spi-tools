package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.project.Project;

public class ProjectArgumentTransformer extends AbstractArgumentTransformer<Project> implements ArgumentTransformer {

	public ProjectArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected Project findReferenceById(String value) {
		return jiraManagersService.getProjectManager().getProjectObj(Long.parseLong(value));
	}

	@Override
	protected Project findReferenceByName(String value) {
		return jiraManagersService.getProjectManager().getProjectObjByName(value);
	}

	@Override
	protected Project findReferenceByKey(String value) {
		return jiraManagersService.getProjectManager().getProjectObjByKey(value);
	}

	@Override
	protected void collectReference(String key, Project project, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectProject(key, project);
	}

	@Override
	protected String getReferenceId(Project project) {
		return String.valueOf(project.getId());
	}

	@Override
	protected String getReferenceName(Project project) {
		return project.getName();
	}

	@Override
	protected String getReferenceKey(Project project) {
		return project.getKey();
	}

	@Override
	protected Optional<Project> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupProject(key);
	}

}