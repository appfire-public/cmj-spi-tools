package com.botronsoft.cmj.spitools.impl.dsl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.botronsoft.cmj.spitools.dsl.ArgumentContainerDescriptor;
import com.botronsoft.cmj.spitools.dsl.ArgumentDescriptor;
import com.botronsoft.cmj.spitools.dsl.ArgumentType;

public class AbstractArgumentDescriptor<T extends ArgumentDescriptor<?>> implements ArgumentDescriptor<T> {

	private final String argumentName;
	private final ArgumentContainerDescriptor container;

	private ArgumentType argumentType = null;
	private ReferenceValueType referenceValueType = ReferenceValueType.ID;
	private String referenceValuePrefix = null;
	private final Set<String> literalValues = new HashSet<>();

	public AbstractArgumentDescriptor(String argumentName, ArgumentContainerDescriptor container) {
		this.argumentName = argumentName;
		this.container = container;
	}

	@Override
	public ArgumentContainerDescriptor and() {
		return container;
	}

	@Override
	public T ofType(ArgumentType argumentType) {
		this.argumentType = argumentType;
		return self();
	}

	@Override
	public T byId() {
		referenceValueType = ReferenceValueType.ID;
		return self();
	}

	@Override
	public T byName() {
		referenceValueType = ReferenceValueType.NAME;
		return self();
	}

	@Override
	public T byKey() {
		referenceValueType = ReferenceValueType.KEY;
		return self();
	}

	@Override
	public T withPrefix(String prefix) {
		referenceValuePrefix = prefix;
		return self();
	}

	@Override
	public T withLiteralValues(String... literalValues) {
		Collections.addAll(this.literalValues, literalValues);
		return self();
	}

	public String getArgumentName() {
		return argumentName;
	}

	public ArgumentType getArgumentType() {
		return argumentType;
	}

	public ReferenceValueType getReferenceValueType() {
		return referenceValueType;
	}

	public String getReferenceValuePrefix() {
		return referenceValuePrefix;
	}

	public Set<String> getLiteralValues() {
		return literalValues;
	}

	@SuppressWarnings("unchecked")
	private T self() {
		return (T) this;
	}
}
