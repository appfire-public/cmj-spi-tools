package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import java.util.Optional;

public class SprintArgumentTransformer extends AbstractArgumentTransformer<Long> implements ArgumentTransformer {

    public SprintArgumentTransformer(JiraManagersService jiraManagersService) {
        super(jiraManagersService);
    }

    @Override
    protected Long findReferenceById(String value) {
        return Long.valueOf(value);
    }

    @Override
    protected Long findReferenceByName(String value) {
        throw new UnsupportedOperationException("Sprints can only be referred by id.");
    }

    @Override
    protected void collectReference(String key, Long reference, ConfigurationReferenceCollector referenceCollector) {
        referenceCollector.collectSprint(key, reference);
    }

    @Override
    protected String getReferenceId(Long reference) {
        return String.valueOf(reference);
    }

    @Override
    protected String getReferenceName(Long reference) {
        throw new UnsupportedOperationException("Sprints can only be referred by id.");
    }

    @Override
    protected Optional<Long> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
        return referenceLookup.lookupSprint(key);
    }
}