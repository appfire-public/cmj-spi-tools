package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;

public class IssueLinkTypeArgumentTransformer extends AbstractArgumentTransformer<IssueLinkType> implements ArgumentTransformer {

	public IssueLinkTypeArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected IssueLinkType findReferenceById(String value) {
		return jiraManagersService.getIssueLinkTypeManager().getIssueLinkType(Long.valueOf(value));
	}

	@Override
	protected IssueLinkType findReferenceByName(String value) {
		IssueLinkTypeManager issueLinkTypeManager = jiraManagersService.getIssueLinkTypeManager();
		List<IssueLinkType> issueLinkTypes = new ArrayList<>(issueLinkTypeManager.getIssueLinkTypesByName(value));
		return findFirstMatch(issueLinkTypes, value, "issue link type");
	}

	@Override
	protected void collectReference(String key, IssueLinkType reference, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectIssueLinkType(key, reference);
	}

	@Override
	protected String getReferenceId(IssueLinkType issueLinkType) {
		return String.valueOf(issueLinkType.getId());
	}

	@Override
	protected String getReferenceName(IssueLinkType issueLinkType) {
		return issueLinkType.getName();
	}

	@Override
	protected Optional<IssueLinkType> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupIssueLinkType(key);
	}
}
