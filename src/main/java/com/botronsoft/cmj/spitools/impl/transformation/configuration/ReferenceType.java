package com.botronsoft.cmj.spitools.impl.transformation.configuration;

/**
 * Describes which attribute of the reference is stored as value in an argument - id, name or key.
 */
public enum ReferenceType {

	BY_ID, BY_NAME, BY_KEY;
}
