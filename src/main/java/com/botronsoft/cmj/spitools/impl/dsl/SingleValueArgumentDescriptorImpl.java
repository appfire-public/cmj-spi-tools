package com.botronsoft.cmj.spitools.impl.dsl;

import com.botronsoft.cmj.spitools.dsl.ArgumentContainerDescriptor;
import com.botronsoft.cmj.spitools.dsl.SingleValueArgumentDescriptor;

/**
 * Class for describing single value argument.
 */
public class SingleValueArgumentDescriptorImpl extends AbstractArgumentDescriptor<SingleValueArgumentDescriptor>
		implements SingleValueArgumentDescriptor {

	public SingleValueArgumentDescriptorImpl(String argumentName, ArgumentContainerDescriptor container) {
		super(argumentName, container);
	}
}
