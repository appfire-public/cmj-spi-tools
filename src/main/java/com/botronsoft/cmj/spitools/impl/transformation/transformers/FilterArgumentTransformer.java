package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;

public class FilterArgumentTransformer extends AbstractArgumentTransformer<SearchRequest> implements ArgumentTransformer {

	public FilterArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected SearchRequest findReferenceById(String value) {
		return jiraManagersService.getSearchRequestManager().getSearchRequestById(Long.valueOf(value));
	}

	@Override
	protected SearchRequest findReferenceByName(String value) {
		SearchRequestManager searchRequestManager = jiraManagersService.getSearchRequestManager();
		List<SearchRequest> filters = new ArrayList<>(searchRequestManager.findByNameIgnoreCase(value));
		return findFirstMatch(filters, value, "filter");
	}

	@Override
	protected void collectReference(String key, SearchRequest reference, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectSearchRequest(key, reference);
	}

	@Override
	protected String getReferenceId(SearchRequest searchRequest) {
		return String.valueOf(searchRequest.getId());
	}

	@Override
	protected String getReferenceName(SearchRequest searchRequest) {
		return searchRequest.getName();
	}

	@Override
	protected Optional<SearchRequest> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupSearchRequest(key);
	}
}
