package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.issue.issuetype.IssueType;

public class IssueTypeArgumentTransformer extends AbstractArgumentTransformer<IssueType> implements ArgumentTransformer {

	public IssueTypeArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected IssueType findReferenceById(String value) {
		return jiraManagersService.getIssueTypeManager().getIssueType(value);
	}

	@Override
	protected IssueType findReferenceByName(String value) {
		IssueTypeManager issueTypeManager = jiraManagersService.getIssueTypeManager();
		List<IssueType> issueTypes = issueTypeManager.getIssueTypes().stream().filter(it -> it.getName().equalsIgnoreCase(value))
				.collect(Collectors.toList());
		return findFirstMatch(issueTypes, value, "issue type");
	}

	@Override
	protected void collectReference(String key, IssueType reference, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectIssueType(key, reference);
	}

	@Override
	protected String getReferenceId(IssueType issueType) {
		return String.valueOf(issueType.getId());
	}

	@Override
	protected String getReferenceName(IssueType issueType) {
		return issueType.getName();
	}

	@Override
	protected Optional<IssueType> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupIssueType(key);
	}
}
