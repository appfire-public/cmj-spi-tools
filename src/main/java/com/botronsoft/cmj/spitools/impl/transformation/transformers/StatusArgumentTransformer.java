package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.config.StatusManager;
import com.atlassian.jira.issue.status.Status;

public class StatusArgumentTransformer extends AbstractArgumentTransformer<Status> implements ArgumentTransformer {

	public StatusArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected Status findReferenceById(String value) {
		return jiraManagersService.getStatusManager().getStatus(value);
	}

	@Override
	protected Status findReferenceByName(String value) {
		StatusManager statusManager = jiraManagersService.getStatusManager();
		List<Status> statuses = statusManager.getStatuses().stream().filter(st -> st.getName().equalsIgnoreCase(value))
				.collect(Collectors.toList());
		return findFirstMatch(statuses, value, "status");
	}

	@Override
	protected void collectReference(String key, Status status, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectStatus(key, status);
	}

	@Override
	protected String getReferenceId(Status status) {
		return String.valueOf(status.getId());
	}

	@Override
	protected String getReferenceName(Status status) {
		return status.getName();
	}

	@Override
	protected Optional<Status> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupStatus(key);
	}

}
