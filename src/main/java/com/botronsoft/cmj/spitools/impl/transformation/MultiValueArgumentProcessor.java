package com.botronsoft.cmj.spitools.impl.transformation;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;
import com.botronsoft.cmj.spitools.impl.transformation.parsers.MultiValueArgumentParser;
import com.botronsoft.cmj.spitools.impl.transformation.producers.MultiValueArgumentProducer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.ArgumentTransformer;

class MultiValueArgumentProcessor implements ArgumentProcessor {

	private static final Logger log = Logger.getLogger(MultiValueArgumentProcessor.class);

	private final Configuration configuration;
	private final MultiValueArgumentParser multiValueParser;
	private final MultiValueArgumentProducer multiValueProducer;
	private final ArgumentTransformer transformer;

	MultiValueArgumentProcessor(Configuration configuration, MultiValueArgumentParser multiValueParser,
			MultiValueArgumentProducer multiValueProducer, ArgumentTransformer transformer) {
		if (!configuration.getSeparator().isPresent()) {
			throw new IllegalArgumentException("No separator for multi-valued argument: " + configuration.getConfigurationPropertyName());
		}

		this.configuration = configuration;
		this.multiValueParser = multiValueParser;
		this.multiValueProducer = multiValueProducer;
		this.transformer = transformer;
	}

	@Override
	public String processArgumentForExport(String value, ExportContext exportContext) {
		Optional<List<String>> singleValuesOptional = multiValueParser.parse(value, configuration);
		if (!singleValuesOptional.isPresent()) {
			log.debug(String.format("Ignoring unparsable value for argument '%s': '%s'", configuration.getConfigurationPropertyName(),
					value));
			return value;
		}

		List<String> singleValues = singleValuesOptional.get();

		return singleValues.stream().map(
				singleValue -> transformer.transformArgumentForExport(singleValue, configuration, exportContext.getReferenceCollector()))
				.collect(Collectors.joining(configuration.getSeparator().get()));
	}

	@Override
	public String processArgumentForImport(String value, ImportContext importContext) {
		Optional<List<String>> singleValuesOptional = multiValueParser.parse(value, configuration);
		if (!singleValuesOptional.isPresent()) {
			log.debug(String.format("Ignoring unparsable value for argument '%s': '%s'", configuration.getConfigurationPropertyName(),
					value));
			return value;
		}

		List<String> singleValues = singleValuesOptional.get();

		List<String> transformedValues = singleValues.stream()
				.map(singleValue -> transformer.transformArgumentForImport(singleValue, configuration, importContext.getReferenceLookup()))
				.collect(Collectors.toList());

		return multiValueProducer.produce(transformedValues, configuration);

	}

}
