package com.botronsoft.cmj.spitools.impl.transformation.configuration;

import java.util.Optional;

import com.botronsoft.cmj.spitools.dsl.ArgumentDescriptor;
import com.botronsoft.cmj.spitools.dsl.ArgumentType;
import com.botronsoft.cmj.spitools.dsl.MultiValueArgumentDescriptor;
import com.botronsoft.cmj.spitools.dsl.MultiValueUserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.dsl.SingleValueArgumentDescriptor;
import com.botronsoft.cmj.spitools.dsl.SingleValueUserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceType;
import com.botronsoft.cmj.spitools.impl.dsl.MultiValueArgumentDescriptorImpl;
import com.botronsoft.cmj.spitools.impl.dsl.MultiValueUserPreferenceDescriptorImpl;
import com.botronsoft.cmj.spitools.impl.dsl.ReferenceValueType;
import com.botronsoft.cmj.spitools.impl.dsl.SingleValueArgumentDescriptorImpl;
import com.botronsoft.cmj.spitools.impl.dsl.SingleValueUserPreferenceDescriptorImpl;

/**
 * Factory for creating {@link Configuration} instances. The factory will transform an {@link ArgumentDescriptor} instance to a
 * {@link Configuration} instance.
 */
public class ConfigurationFactory {

	private ConfigurationFactory() {
		// prevent instantiation
	}

	/**
	 * Creates a configuration from a {@link SingleValueArgumentDescriptor} instance.
	 *
	 * @param argumentDescriptor
	 *            the argument descriptor.
	 * @return the {@link Configuration} instance.
	 */
	public static Configuration createConfigurationForSingleValueArgumentDescriptor(SingleValueArgumentDescriptor argumentDescriptor) {
		SingleValueArgumentDescriptorImpl descriptorImpl = (SingleValueArgumentDescriptorImpl) argumentDescriptor;
		return new Configuration(descriptorImpl.getArgumentName(), toConfigurationPropertyType(descriptorImpl.getArgumentType()),
				toReferenceType(descriptorImpl.getReferenceValueType()), descriptorImpl.getReferenceValuePrefix(),
				descriptorImpl.getLiteralValues());
	}

	/**
	 * Creates a configuration from a {@link MultiValueArgumentDescriptor} instance.
	 *
	 * @param argumentDescriptor
	 *            the argument descriptor.
	 * @return the {@link Configuration} instance.
	 */
	public static Configuration createConfigurationForMultiValueArgumentDescriptor(MultiValueArgumentDescriptor argumentDescriptor) {
		MultiValueArgumentDescriptorImpl descriptorImpl = (MultiValueArgumentDescriptorImpl) argumentDescriptor;
		return new Configuration(descriptorImpl.getArgumentName(), toConfigurationPropertyType(descriptorImpl.getArgumentType()),
				toReferenceType(descriptorImpl.getReferenceValueType()), descriptorImpl.getReferenceValuePrefix(),
				descriptorImpl.getLiteralValues(), Optional.ofNullable(descriptorImpl.getSeparator()));
	}

	/**
	 * Creates a configuration from a {@link SingleValueUserPreferenceDescriptor} instance.
	 *
	 * @param userPreferenceDescriptor
	 *            the user preference descriptor.
	 * @return the {@link Configuration} instance.
	 */
	public static Configuration createConfigurationForSingleValueUserPreferenceDescriptor(
			SingleValueUserPreferenceDescriptor userPreferenceDescriptor) {
		SingleValueUserPreferenceDescriptorImpl descriptorImpl = (SingleValueUserPreferenceDescriptorImpl) userPreferenceDescriptor;
		return new Configuration(descriptorImpl.getUserPreferenceName(),
				toConfigurationPropertyType(descriptorImpl.getUserPreferenceType()),
				toReferenceType(descriptorImpl.getReferenceValueType()), descriptorImpl.getReferenceValuePrefix(),
				descriptorImpl.getLiteralValues());
	}

	/**
	 * Creates a configuration from a {@link MultiValueUserPreferenceDescriptor} instance.
	 *
	 * @param userPreferenceDescriptor
	 *            the user preference descriptor.
	 * @return the {@link Configuration} instance.
	 */
	public static Configuration createConfigurationForMultiValueUserPreferenceDescriptor(
			MultiValueUserPreferenceDescriptor userPreferenceDescriptor) {
		MultiValueUserPreferenceDescriptorImpl descriptorImpl = (MultiValueUserPreferenceDescriptorImpl) userPreferenceDescriptor;
		return new Configuration(descriptorImpl.getUserPreferenceName(),
				toConfigurationPropertyType(descriptorImpl.getUserPreferenceType()),
				toReferenceType(descriptorImpl.getReferenceValueType()), descriptorImpl.getReferenceValuePrefix(),
				descriptorImpl.getLiteralValues(), Optional.ofNullable(descriptorImpl.getSeparator()));
	}

	private static ConfigurationPropertyType toConfigurationPropertyType(ArgumentType argumentType) {
		return ConfigurationPropertyType.valueOf(argumentType.name());
	}

	private static ConfigurationPropertyType toConfigurationPropertyType(UserPreferenceType userPreferenceType) {
		return ConfigurationPropertyType.valueOf(userPreferenceType.name());
	}

	private static ReferenceType toReferenceType(ReferenceValueType referenceValueType) {
		switch (referenceValueType) {
		case ID:
			return ReferenceType.BY_ID;
		case NAME:
			return ReferenceType.BY_NAME;
		case KEY:
			return ReferenceType.BY_KEY;
		default:
			throw new IllegalArgumentException("Unknown reference value type: " + referenceValueType);
		}
	}
}
