package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.issue.resolution.Resolution;

public class ResolutionArgumentTransformer extends AbstractArgumentTransformer<Resolution> implements ArgumentTransformer {

	public ResolutionArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected Resolution findReferenceById(String value) {
		return jiraManagersService.getResolutionManager().getResolution(value);
	}

	@Override
	protected Resolution findReferenceByName(String value) {
		return jiraManagersService.getResolutionManager().getResolutionByName(value);
	}

	@Override
	protected void collectReference(String key, Resolution resolution, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectResolution(key, resolution);
	}

	@Override
	protected String getReferenceId(Resolution resolution) {
		return String.valueOf(resolution.getId());
	}

	@Override
	protected String getReferenceName(Resolution resolution) {
		return resolution.getName();
	}

	@Override
	protected Optional<Resolution> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupResolution(key);
	}
}
