package com.botronsoft.cmj.spitools.impl.dsl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.botronsoft.cmj.spitools.dsl.MultiValueUserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.dsl.SingleValueUserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceContainerDescriptor;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceDescriptor;

public class UserPreferenceContainerDescriptorImpl implements UserPreferenceContainerDescriptor {

	private final Map<String, UserPreferenceDescriptor<?>> userPreferenceDescriptorMap = new HashMap<>();

	@Override
	public SingleValueUserPreferenceDescriptor hasSingleValueUserPreference(String userPreferenceName) {
		SingleValueUserPreferenceDescriptor descriptor = new SingleValueUserPreferenceDescriptorImpl(userPreferenceName, this);
		userPreferenceDescriptorMap.put(userPreferenceName, descriptor);
		return descriptor;
	}

	@Override
	public MultiValueUserPreferenceDescriptor hasMultiValueUserPreference(String userPreferenceName) {
		MultiValueUserPreferenceDescriptor descriptor = new MultiValueUserPreferenceDescriptorImpl(userPreferenceName, this);
		userPreferenceDescriptorMap.put(userPreferenceName, descriptor);
		return descriptor;
	}

	public Optional<UserPreferenceDescriptor<?>> getUserPreferenceDescriptor(String userPreferenceName) {
		UserPreferenceDescriptor<?> userPreferenceDescriptor = userPreferenceDescriptorMap.get(userPreferenceName);
		if (userPreferenceDescriptor == null) {
			return Optional.empty();
		}

		return Optional.of(userPreferenceDescriptor);
	}

	public Map<String, UserPreferenceDescriptor<?>> getUserPreferenceDescriptors() {
		return userPreferenceDescriptorMap;
	}
}
