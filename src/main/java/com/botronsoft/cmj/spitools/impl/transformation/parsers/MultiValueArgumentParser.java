package com.botronsoft.cmj.spitools.impl.transformation.parsers;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;
import com.google.common.collect.Lists;

/**
 * Takes care of parsing a multi-value property. The value will be split using the separator and quotes will be removed.
 */
public class MultiValueArgumentParser {

	private final SingleValueArgumentParser singleValueArgumentParser;

	public MultiValueArgumentParser() {
		singleValueArgumentParser = new SingleValueArgumentParser();
	}

	public Optional<List<String>> parse(String value, Configuration configuration) {
		if ((value == null) || configuration.getLiteralValues().contains(value) || !configuration.getSeparator().isPresent()) {
			return Optional.empty();
		}

		List<String> result = Lists.newArrayList();
		if (!value.isEmpty()) {
			String separator = configuration.getSeparator().get();
			String regex = Pattern.quote(separator) // the separator to split
					+ "(?x)                      " // enable comments, ignore white spaces
					+ "(?=                       " // start positive look ahead
					+ "  (                       " // start group 1
					+ "    [^\"]*                " // match "other than quote" zero or more times
					+ "    \"                    " // match quote
					+ "    [^\"]*                " // match "other than quote" zero or more times
					+ "    \"                    " // match quote
					+ "  )*                      " // end group 1 and repeat it 0 or more times
					+ "  [^\"]*                  " // match "other than quote"
					+ "  $                       " // match the end of the string
					+ ")                         ";// stop positive lookahead
			for (String singleValue : value.split(regex)) {
				String parsedValue = singleValue.trim().replaceAll("\"", "");
				result.add(singleValueArgumentParser.removePrefix(parsedValue, configuration));
			}
		}

		return Optional.of(result);
	}
}
