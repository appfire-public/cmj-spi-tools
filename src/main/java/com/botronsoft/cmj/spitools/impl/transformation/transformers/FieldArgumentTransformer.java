package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.issue.fields.Field;

public class FieldArgumentTransformer extends AbstractArgumentTransformer<Field> implements ArgumentTransformer {

	public FieldArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected Field findReferenceById(String value) {
		return jiraManagersService.getCustomFieldManager().getCustomFieldObject(value);
	}

	@Override
	protected Field findReferenceByName(String value) {
		return jiraManagersService.getCustomFieldManager().getCustomFieldObjectByName(value);
	}

	@Override
	protected void collectReference(String key, Field reference, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectField(key, reference);
	}

	@Override
	protected String getReferenceId(Field field) {
		return field.getId();
	}

	@Override
	protected String getReferenceName(Field field) {
		return field.getName();
	}

	@Override
	protected Optional<Field> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupField(key);
	}

}