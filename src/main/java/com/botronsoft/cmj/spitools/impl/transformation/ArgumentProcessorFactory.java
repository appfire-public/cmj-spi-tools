package com.botronsoft.cmj.spitools.impl.transformation;

import java.util.HashMap;
import java.util.Map;

import com.botronsoft.cmj.spitools.dsl.ArgumentDescriptor;
import com.botronsoft.cmj.spitools.dsl.MultiValueArgumentDescriptor;
import com.botronsoft.cmj.spitools.dsl.MultiValueUserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.dsl.SingleValueArgumentDescriptor;
import com.botronsoft.cmj.spitools.dsl.SingleValueUserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.ConfigurationFactory;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.ConfigurationPropertyType;
import com.botronsoft.cmj.spitools.impl.transformation.parsers.MultiValueArgumentParser;
import com.botronsoft.cmj.spitools.impl.transformation.parsers.SingleValueArgumentParser;
import com.botronsoft.cmj.spitools.impl.transformation.producers.MultiValueArgumentProducer;
import com.botronsoft.cmj.spitools.impl.transformation.producers.SingleValueArgumentProducer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.AgileBoardArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.ArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.EventTypeArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.FieldArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.FieldContextArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.FieldOptionArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.FilterArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.GroupArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.IssueLinkTypeArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.IssueTypeArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.PriorityArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.ProjectArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.ProjectComponentArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.ProjectRoleArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.ProjectVersionArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.QueryArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.RequestTypeArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.ResolutionArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.SecurityLevelArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.SprintArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.StatusArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.UserArgumentTransformer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.WorkflowArgumentTransformer;

/**
 * Singleton factory for creating {@link ArgumentProcessor} instances.
 */
public class ArgumentProcessorFactory {

	private final Map<ConfigurationPropertyType, ArgumentTransformer> transformersClassesMap = new HashMap<>();

	public ArgumentProcessorFactory(JiraManagersService jiraManagersService) {
		initializeTransformersMap(jiraManagersService);
	}

	private void initializeTransformersMap(JiraManagersService jiraManagersService) {
		transformersClassesMap.put(ConfigurationPropertyType.FIELD, new FieldArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.FIELD_OPTION, new FieldOptionArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.FIELD_CONTEXT, new FieldContextArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.USER, new UserArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.GROUP, new GroupArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.STATUS, new StatusArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.ISSUE_TYPE, new IssueTypeArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.ISSUE_LINK_TYPE, new IssueLinkTypeArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.PRIORITY, new PriorityArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.EVENT_TYPE, new EventTypeArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.RESOLUTION, new ResolutionArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.PROJECT, new ProjectArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.PROJECT_VERSION, new ProjectVersionArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.PROJECT_COMPONENT,
				new ProjectComponentArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.PROJECT_ROLE, new ProjectRoleArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.SEARCH_REQUEST, new FilterArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.QUERY, new QueryArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.SECURITY_LEVEL, new SecurityLevelArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.REQUEST_TYPE, new RequestTypeArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.SPRINT, new SprintArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.AGILE_BOARD, new AgileBoardArgumentTransformer(jiraManagersService));
		transformersClassesMap.put(ConfigurationPropertyType.WORKFLOW, new WorkflowArgumentTransformer(jiraManagersService));
	}

	public ArgumentProcessor createProcessor(ArgumentDescriptor descriptor) {
		if (descriptor instanceof SingleValueArgumentDescriptor) {
			return createSingleValueProcessor((SingleValueArgumentDescriptor) descriptor);
		} else if (descriptor instanceof MultiValueArgumentDescriptor) {
			return createMultiValueProcessor((MultiValueArgumentDescriptor) descriptor);
		} else {
			throw new IllegalArgumentException("Unsupported argument descriptor type: " + descriptor);
		}
	}

	private SingleValueArgumentProcessor createSingleValueProcessor(SingleValueArgumentDescriptor descriptor) {
		Configuration configuration = ConfigurationFactory.createConfigurationForSingleValueArgumentDescriptor(descriptor);
		return new SingleValueArgumentProcessor(configuration, new SingleValueArgumentParser(), new SingleValueArgumentProducer(),
				getArgumentTransformer(configuration.getConfigurationPropertyType()));
	}

	private MultiValueArgumentProcessor createMultiValueProcessor(MultiValueArgumentDescriptor descriptor) {
		Configuration configuration = ConfigurationFactory.createConfigurationForMultiValueArgumentDescriptor(descriptor);
		return new MultiValueArgumentProcessor(configuration, new MultiValueArgumentParser(), new MultiValueArgumentProducer(),
				getArgumentTransformer(configuration.getConfigurationPropertyType()));
	}

	public ArgumentProcessor createProcessor(UserPreferenceDescriptor descriptor) {
		if (descriptor instanceof SingleValueUserPreferenceDescriptor) {
			return createSingleValueProcessor((SingleValueUserPreferenceDescriptor) descriptor);
		} else if (descriptor instanceof MultiValueUserPreferenceDescriptor) {
			return createMultiValueProcessor((MultiValueUserPreferenceDescriptor) descriptor);
		} else {
			throw new IllegalArgumentException("Unsupported user preference descriptor type: " + descriptor);
		}
	}

	private SingleValueArgumentProcessor createSingleValueProcessor(SingleValueUserPreferenceDescriptor descriptor) {
		Configuration configuration = ConfigurationFactory.createConfigurationForSingleValueUserPreferenceDescriptor(descriptor);
		return new SingleValueArgumentProcessor(configuration, new SingleValueArgumentParser(), new SingleValueArgumentProducer(),
				getArgumentTransformer(configuration.getConfigurationPropertyType()));
	}

	private MultiValueArgumentProcessor createMultiValueProcessor(MultiValueUserPreferenceDescriptor descriptor) {
		Configuration configuration = ConfigurationFactory.createConfigurationForMultiValueUserPreferenceDescriptor(descriptor);
		return new MultiValueArgumentProcessor(configuration, new MultiValueArgumentParser(), new MultiValueArgumentProducer(),
				getArgumentTransformer(configuration.getConfigurationPropertyType()));
	}

	private ArgumentTransformer getArgumentTransformer(ConfigurationPropertyType type) {
		return transformersClassesMap.get(type);
	}
}
