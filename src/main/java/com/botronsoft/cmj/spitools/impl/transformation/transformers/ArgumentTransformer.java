package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;

/**
 * Base interface for all transformers that take care of transforming an argument value to an object in Jira and vice-versa.
 */
public interface ArgumentTransformer {

	/**
	 * Transforms argument value for export.
	 *
	 * @param value
	 *            value to be transformed
	 * @param configuration
	 *            the transformation configuration
	 * @param referenceCollector
	 *            instance of {@link ConfigurationReferenceCollector} from the SPI.
	 * @return transformed value
	 */
	String transformArgumentForExport(String value, Configuration configuration, ConfigurationReferenceCollector referenceCollector);

	/**
	 * Transforms argument value for import.
	 *
	 * @param value
	 *            value to be transformed
	 * @param configuration
	 *            the transformation configuration
	 * @param referenceLookup
	 *            instance of {@link ConfigurationReferenceLookup} from the SPI.
	 * @return transformed value
	 */
	String transformArgumentForImport(String value, Configuration configuration, ConfigurationReferenceLookup referenceLookup);

}
