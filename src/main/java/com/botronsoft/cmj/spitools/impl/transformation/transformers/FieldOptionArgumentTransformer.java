package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.dsl.ArgumentType;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.issue.customfields.option.Option;

public class FieldOptionArgumentTransformer extends AbstractArgumentTransformer<Option> implements ArgumentTransformer {

	public FieldOptionArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected Option findReferenceById(String value) {
		return jiraManagersService.getOptionsManager().findByOptionId(Long.valueOf(value));
	}

	@Override
	protected Option findReferenceByName(String value) {
		throw new IllegalArgumentException(getUnsupportedReferenceTypeMessage());
	}

	@Override
	protected void collectReference(String key, Option reference, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectFieldOption(key, reference);
	}

	@Override
	protected String getReferenceId(Option option) {
		return String.valueOf(option.getOptionId());
	}

	@Override
	protected String getReferenceName(Option option) {
		throw new IllegalArgumentException(getUnsupportedReferenceTypeMessage());
	}

	@Override
	protected Optional<Option> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupFieldOption(key);
	}

	private String getUnsupportedReferenceTypeMessage() {
		return "Unsupported reference type '" + ReferenceType.BY_NAME + "' for argument of type '" + ArgumentType.FIELD_OPTION + "'.";
	}
}
