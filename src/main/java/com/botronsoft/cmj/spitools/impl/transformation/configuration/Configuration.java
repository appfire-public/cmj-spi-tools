package com.botronsoft.cmj.spitools.impl.transformation.configuration;

import java.util.Optional;
import java.util.Set;

/**
 * The configuration which is extracted from an argument descriptor. See {@link ConfigurationFactory}.
 */
public class Configuration {

	private final String configurationPropertyName;
	private final ConfigurationPropertyType configurationPropertyType;
	private final ReferenceType referenceType;
	private final String referenceValuePrefix;
	private final Set<String> literalValues;
	private final Optional<String> separator;

	Configuration(String configurationPropertyName, ConfigurationPropertyType configurationPropertyType, ReferenceType referenceType,
			String referenceValuePrefix, Set<String> literalValues) {
		this(configurationPropertyName, configurationPropertyType, referenceType, referenceValuePrefix, literalValues, Optional.empty());
	}

	Configuration(String configurationPropertyName, ConfigurationPropertyType configurationPropertyType, ReferenceType referenceType,
			String referenceValuePrefix, Set<String> literalValues, Optional<String> separator) {
		this.configurationPropertyName = configurationPropertyName;
		this.configurationPropertyType = configurationPropertyType;
		this.referenceType = referenceType;
		this.referenceValuePrefix = referenceValuePrefix;
		this.literalValues = literalValues;
		this.separator = separator;
	}

	public String getConfigurationPropertyName() {
		return configurationPropertyName;
	}

	public ConfigurationPropertyType getConfigurationPropertyType() {
		return configurationPropertyType;
	}

	public ReferenceType getReferenceType() {
		return referenceType;
	}

	public String getReferenceValuePrefix() {
		return referenceValuePrefix;
	}

	public Set<String> getLiteralValues() {
		return literalValues;
	}

	public Optional<String> getSeparator() {
		return separator;
	}
}
