package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.config.PriorityManager;
import com.atlassian.jira.issue.priority.Priority;

public class PriorityArgumentTransformer extends AbstractArgumentTransformer<Priority> implements ArgumentTransformer {

	public PriorityArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected Priority findReferenceById(String value) {
		return jiraManagersService.getPriorityManager().getPriority(value);
	}

	@Override
	protected Priority findReferenceByName(String value) {
		PriorityManager priorityManager = jiraManagersService.getPriorityManager();
		List<Priority> priorities = priorityManager.getPriorities().stream().filter(pr -> pr.getName().equalsIgnoreCase(value))
				.collect(Collectors.toList());
		return findFirstMatch(priorities, value, "priority");
	}

	@Override
	protected void collectReference(String key, Priority priority, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectPriority(key, priority);
	}

	@Override
	protected String getReferenceId(Priority priority) {
		return String.valueOf(priority.getId());
	}

	@Override
	protected String getReferenceName(Priority priority) {
		return priority.getName();
	}

	@Override
	protected Optional<Priority> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupPriority(key);
	}

}
