package com.botronsoft.cmj.spitools.impl.transformation.configuration;

/**
 * Represents the different types of configuration elements that can be referenced in arguments.
 */
public enum ConfigurationPropertyType {

	FIELD,

	FIELD_OPTION,

	FIELD_CONTEXT,

	USER,

	GROUP,

	ISSUE_TYPE,

	ISSUE_LINK_TYPE,

	STATUS,

	RESOLUTION,

	PRIORITY,

	EVENT_TYPE,

	SEARCH_REQUEST,

	QUERY,

	PROJECT,

	PROJECT_VERSION,

	PROJECT_COMPONENT,

	PROJECT_ROLE,

	SECURITY_LEVEL,

	REQUEST_TYPE,

	SPRINT,

	AGILE_BOARD,

	WORKFLOW
}
