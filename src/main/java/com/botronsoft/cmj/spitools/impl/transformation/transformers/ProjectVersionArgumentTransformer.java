package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Collection;
import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.project.version.Version;

public class ProjectVersionArgumentTransformer extends AbstractArgumentTransformer<Version> implements ArgumentTransformer {

	public ProjectVersionArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected Version findReferenceById(String value) {
		return jiraManagersService.getProjectVersionManager().getVersion(Long.valueOf(value));
	}

	@Override
	protected Version findReferenceByName(String value) {
		Collection<Version> projectVersions = jiraManagersService.getProjectVersionManager().getVersionsByName(value);
		return findFirstMatch(projectVersions, value, "project version");
	}

	@Override
	protected void collectReference(String key, Version projectVersion, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectProjectVersion(key, projectVersion);
	}

	@Override
	protected String getReferenceId(Version projectVersion) {
		return projectVersion.getId().toString();
	}

	@Override
	protected String getReferenceName(Version projectVersion) {
		return projectVersion.getName();
	}

	@Override
	protected Optional<Version> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupProjectVersion(key);
	}

}
