package com.botronsoft.cmj.spitools.impl.dsl;

import com.botronsoft.cmj.spitools.dsl.SingleValueUserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceContainerDescriptor;

/**
 * Class for describing single value user preference.
 */
public class SingleValueUserPreferenceDescriptorImpl extends AbstractUserPreferenceDescriptor<SingleValueUserPreferenceDescriptor>
		implements SingleValueUserPreferenceDescriptor {

	public SingleValueUserPreferenceDescriptorImpl(String userPreferenceName, UserPreferenceContainerDescriptor container) {
		super(userPreferenceName, container);
	}
}
