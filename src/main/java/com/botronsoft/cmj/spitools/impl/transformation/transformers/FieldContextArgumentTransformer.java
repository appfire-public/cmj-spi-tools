package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.dsl.ArgumentType;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.issue.fields.config.FieldConfigScheme;

public class FieldContextArgumentTransformer extends AbstractArgumentTransformer<FieldConfigScheme> implements ArgumentTransformer {

	public FieldContextArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected FieldConfigScheme findReferenceById(String value) {
		return jiraManagersService.getFieldConfigSchemeManager().getFieldConfigScheme(Long.valueOf(value));
	}

	@Override
	protected FieldConfigScheme findReferenceByName(String value) {
		throw new IllegalArgumentException(getUnsupportedReferenceTypeMessage());
	}

	@Override
	protected void collectReference(String key, FieldConfigScheme reference, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectFieldContext(key, reference);
	}

	@Override
	protected String getReferenceId(FieldConfigScheme reference) {
		return String.valueOf(reference.getId());
	}

	@Override
	protected String getReferenceName(FieldConfigScheme reference) {
		throw new IllegalArgumentException(getUnsupportedReferenceTypeMessage());
	}

	@Override
	protected Optional<FieldConfigScheme> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupFieldContext(key);
	}

	private String getUnsupportedReferenceTypeMessage() {
		return "Unsupported reference type '" + ReferenceType.BY_NAME + "' for argument of type '" + ArgumentType.FIELD_CONTEXT + "'.";
	}
}
