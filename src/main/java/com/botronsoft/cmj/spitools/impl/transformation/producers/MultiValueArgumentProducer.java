package com.botronsoft.cmj.spitools.impl.transformation.producers;

import java.util.List;
import java.util.stream.Collectors;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;

public class MultiValueArgumentProducer {

	private final SingleValueArgumentProducer singleValueProducer;

	public MultiValueArgumentProducer() {
		singleValueProducer = new SingleValueArgumentProducer();
	}

	public String produce(List<String> values, Configuration configuration) {
		return values.stream().map(value -> singleValueProducer.produce(value, configuration))
				.collect(Collectors.joining(configuration.getSeparator().get()));
	}
}
