package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Collection;
import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.issue.security.IssueSecurityLevel;

public class SecurityLevelArgumentTransformer extends AbstractArgumentTransformer<IssueSecurityLevel> implements ArgumentTransformer {

	public SecurityLevelArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected IssueSecurityLevel findReferenceById(String value) {
		return jiraManagersService.getIssueSecurityLevelManager().getSecurityLevel(Long.valueOf(value));
	}

	@Override
	protected IssueSecurityLevel findReferenceByName(String value) {
		Collection<IssueSecurityLevel> securityLevels = jiraManagersService.getIssueSecurityLevelManager()
				.getIssueSecurityLevelsByName(value);
		return findFirstMatch(securityLevels, value, "issue security level");
	}

	@Override
	protected void collectReference(String key, IssueSecurityLevel reference, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectSecurityLevel(key, reference);
	}

	@Override
	protected String getReferenceId(IssueSecurityLevel reference) {
		return String.valueOf(reference.getId());
	}

	@Override
	protected String getReferenceName(IssueSecurityLevel reference) {
		return reference.getName();
	}

	@Override
	protected Optional<IssueSecurityLevel> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupSecurityLevel(key);
	}
}
