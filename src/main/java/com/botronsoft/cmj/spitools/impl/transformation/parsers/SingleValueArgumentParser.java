package com.botronsoft.cmj.spitools.impl.transformation.parsers;

import java.util.Optional;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;

/**
 * Takes care of parsing a single-value property.
 */
public class SingleValueArgumentParser {

	public Optional<String> parse(String value, Configuration configuration) {
		Optional<String> result = Optional.empty();
		if ((value != null) && !value.isEmpty() && !configuration.getLiteralValues().contains(value)) {
			result = Optional.of(removePrefix(value, configuration));
		}

		return result;
	}

	public String removePrefix(String value, Configuration configuration) {
		String prefix = configuration.getReferenceValuePrefix();
		if ((prefix != null) && (prefix.length() > 0) && value.startsWith(prefix)) {
			return value.substring(prefix.length());
		}
		return value;
	}
}
