package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Collection;
import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.bc.project.component.ProjectComponent;

public class ProjectComponentArgumentTransformer extends AbstractArgumentTransformer<ProjectComponent> implements ArgumentTransformer {

	public ProjectComponentArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected ProjectComponent findReferenceById(String value) {
		return jiraManagersService.getProjectComponentManager().getProjectComponent(Long.valueOf(value));
	}

	@Override
	protected ProjectComponent findReferenceByName(String value) {
		Collection<ProjectComponent> projectComponents = jiraManagersService.getProjectComponentManager()
				.findByComponentNameCaseInSensitive(value);
		return findFirstMatch(projectComponents, value, "project component");
	}

	@Override
	protected void collectReference(String key, ProjectComponent projectComponent, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectProjectComponent(key, projectComponent);
	}

	@Override
	protected String getReferenceId(ProjectComponent projectComponent) {
		return projectComponent.getId().toString();
	}

	@Override
	protected String getReferenceName(ProjectComponent projectComponent) {
		return projectComponent.getName();
	}

	@Override
	protected Optional<ProjectComponent> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupProjectComponent(key);
	}

}
