package com.botronsoft.cmj.spitools.impl.dsl;

public enum ReferenceValueType {
    ID, NAME, KEY;
}