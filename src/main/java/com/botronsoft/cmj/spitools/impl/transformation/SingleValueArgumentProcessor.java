package com.botronsoft.cmj.spitools.impl.transformation;

import java.util.Optional;

import org.apache.log4j.Logger;

import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;
import com.botronsoft.cmj.spitools.impl.transformation.parsers.SingleValueArgumentParser;
import com.botronsoft.cmj.spitools.impl.transformation.producers.SingleValueArgumentProducer;
import com.botronsoft.cmj.spitools.impl.transformation.transformers.ArgumentTransformer;

class SingleValueArgumentProcessor implements ArgumentProcessor {

	private static final Logger log = Logger.getLogger(SingleValueArgumentProcessor.class);

	private final Configuration configuration;
	private final SingleValueArgumentParser singleValueParser;
	private final SingleValueArgumentProducer singleValueProducer;
	private final ArgumentTransformer transformer;

	SingleValueArgumentProcessor(Configuration configuration, SingleValueArgumentParser singleValueParser,
			SingleValueArgumentProducer singleValueProducer, ArgumentTransformer transformer) {
		this.configuration = configuration;
		this.singleValueParser = singleValueParser;
		this.singleValueProducer = singleValueProducer;
		this.transformer = transformer;
	}

	@Override
	public String processArgumentForExport(String value, ExportContext exportContext) {
		Optional<String> valueOptional = singleValueParser.parse(value, configuration);
		if (!valueOptional.isPresent()) {
			log.debug(String.format("Ignoring value for argument '%s': '%s'", configuration.getConfigurationPropertyName(), value));
			return value;
		}

		return transformer.transformArgumentForExport(valueOptional.get(), configuration, exportContext.getReferenceCollector());
	}

	@Override
	public String processArgumentForImport(String value, ImportContext importContext) {
		Optional<String> valueOptional = singleValueParser.parse(value, configuration);
		if (!valueOptional.isPresent()) {
			log.debug(String.format("Ignoring value for argument '%s': '%s'", configuration.getConfigurationPropertyName(), value));
			return value;
		}

		String transformedValue = transformer.transformArgumentForImport(value, configuration, importContext.getReferenceLookup());

		return singleValueProducer.produce(transformedValue, configuration);
	}
}
