package com.botronsoft.cmj.spitools.impl;

import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.config.PriorityManager;
import com.atlassian.jira.config.ResolutionManager;
import com.atlassian.jira.config.StatusManager;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.WorkflowManager;

/**
 * Bridge to Jira services.
 */
public interface JiraManagersService {

	CustomFieldManager getCustomFieldManager();

	OptionsManager getOptionsManager();

	FieldConfigSchemeManager getFieldConfigSchemeManager();

	SearchRequestManager getSearchRequestManager();

	GroupManager getGroupManager();

	IssueLinkTypeManager getIssueLinkTypeManager();

	IssueTypeManager getIssueTypeManager();

	PriorityManager getPriorityManager();

	EventTypeManager getEventTypeManager();

	ProjectManager getProjectManager();

	VersionManager getProjectVersionManager();

	ProjectComponentManager getProjectComponentManager();

	ProjectRoleManager getProjectRoleManager();

	JqlQueryParser getJqlQueryParser();

	ResolutionManager getResolutionManager();

	StatusManager getStatusManager();

	UserManager getUserManager();

	JqlStringSupport getJqlStringSupport();

	IssueSecurityLevelManager getIssueSecurityLevelManager();

	WorkflowManager getWorkflowManager();
}
