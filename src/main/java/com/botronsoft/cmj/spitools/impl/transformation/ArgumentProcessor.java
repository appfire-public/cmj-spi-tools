package com.botronsoft.cmj.spitools.impl.transformation;

import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;

/**
 * The argument processor will process a single argument for export or import.
 */
public interface ArgumentProcessor {

	/**
	 * Process an argument for export. This value will be parsed, and references to configuration elements extracted and recorded.
	 *
	 * @param value
	 *            the argument value.
	 * @param exportContext
	 *            the export context. See {@link ExportContext}.
	 * @return the processed value to be stored for transfer.
	 */
	String processArgumentForExport(String value, ExportContext exportContext);

	/**
	 * Process an argument for import. References to configuration elements will be resolved and a new value will be generated.
	 *
	 * @param value
	 *            the argument value - the same value which was returned by {@link #processArgumentForExport(String, ExportContext)}.
	 * @param importContext
	 *            the import context. See {@link ImportContext}.
	 * @return the generated value which will be recorded as the new argument value.
	 */
	String processArgumentForImport(String value, ImportContext importContext);
}
