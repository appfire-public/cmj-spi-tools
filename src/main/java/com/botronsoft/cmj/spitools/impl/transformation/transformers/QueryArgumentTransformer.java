package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Optional;

import org.apache.log4j.Logger;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;

import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.query.Query;

public class QueryArgumentTransformer implements ArgumentTransformer {

	private static final Logger log = Logger.getLogger(AbstractArgumentTransformer.class);

	private final JiraManagersService jiraManagersService;

	public QueryArgumentTransformer(JiraManagersService jiraManagersService) {
		this.jiraManagersService = jiraManagersService;
	}

	@Override
	public String transformArgumentForExport(String value, Configuration configuration,
			ConfigurationReferenceCollector referenceCollector) {
		Query parsedQuery;

		try {
			parsedQuery = jiraManagersService.getJqlQueryParser().parseQuery(value);
		} catch (JqlParseException e) {
			log.debug("Unable to parse JQL query: " + value, e);
			return value;
		}

		referenceCollector.collectQuery(value, parsedQuery);

		log.debug(String.format("Successfully extracted query from value '%s' for argument '%s'", value,
				configuration.getConfigurationPropertyName()));
		return value;
	}

	@Override
	public String transformArgumentForImport(String value, Configuration configuration, ConfigurationReferenceLookup referenceLookup) {
		Optional<Query> query = referenceLookup.lookupQuery(value);
		if (!query.isPresent()) {
			log.debug(String.format("Unable to resolve query from value '%s' for argument '%s'", value,
					configuration.getConfigurationPropertyName()));
			return value;
		}

		log.debug(String.format("Successfully resolved query from value '%s' for argument '%s'", value,
				configuration.getConfigurationPropertyName()));

		JqlStringSupport jqlStringSupport = jiraManagersService.getJqlStringSupport();
		return jqlStringSupport.generateJqlString(query.get());
	}
}
