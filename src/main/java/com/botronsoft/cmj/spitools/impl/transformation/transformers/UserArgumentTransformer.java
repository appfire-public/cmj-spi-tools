package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.user.ApplicationUser;

public class UserArgumentTransformer extends AbstractArgumentTransformer<ApplicationUser> implements ArgumentTransformer {

	public UserArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected ApplicationUser findReferenceById(String value) {
		return findReferenceByKey(value);
	}

	@Override
	protected ApplicationUser findReferenceByName(String value) {
		return jiraManagersService.getUserManager().getUserByName(value);
	}

	@Override
	protected ApplicationUser findReferenceByKey(String value) {
		return jiraManagersService.getUserManager().getUserByKey(value);
	}

	@Override
	protected void collectReference(String key, ApplicationUser applicationUser, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectUser(key, applicationUser);
	}

	@Override
	protected String getReferenceId(ApplicationUser applicationUser) {
		return getReferenceKey(applicationUser);
	}

	@Override
	protected String getReferenceName(ApplicationUser applicationUser) {
		return applicationUser.getName();
	}

	@Override
	protected String getReferenceKey(ApplicationUser applicationUser) {
		return applicationUser.getKey();
	}

	@Override
	protected Optional<ApplicationUser> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupUser(key);
	}
}
