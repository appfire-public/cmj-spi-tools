package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.type.EventTypeManager;

public class EventTypeArgumentTransformer extends AbstractArgumentTransformer<EventType> implements ArgumentTransformer {

	public EventTypeArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected EventType findReferenceById(String value) {
		return jiraManagersService.getEventTypeManager().getEventType(Long.valueOf(value));
	}

	@Override
	protected EventType findReferenceByName(String value) {
		EventTypeManager eventTypeManager = jiraManagersService.getEventTypeManager();
		List<EventType> eventTypes = eventTypeManager.getEventTypes().stream().filter(et -> et.getName().equalsIgnoreCase(value))
				.collect(Collectors.toList());
		return findFirstMatch(eventTypes, value, "event type");
	}

	@Override
	protected void collectReference(String key, EventType eventType, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectEventType(key, eventType);
	}

	@Override
	protected String getReferenceId(EventType eventType) {
		return String.valueOf(eventType.getId());
	}

	@Override
	protected String getReferenceName(EventType eventType) {
		return eventType.getName();
	}

	@Override
	protected Optional<EventType> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupEventType(key);
	}
}
