package com.botronsoft.cmj.spitools.impl.dsl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.botronsoft.cmj.spitools.dsl.ArgumentContainerDescriptor;
import com.botronsoft.cmj.spitools.dsl.ArgumentDescriptor;
import com.botronsoft.cmj.spitools.dsl.MultiValueArgumentDescriptor;
import com.botronsoft.cmj.spitools.dsl.SingleValueArgumentDescriptor;

public class ArgumentContainerDescriptorImpl implements ArgumentContainerDescriptor {

	private final Map<String, ArgumentDescriptor<?>> argumentDescriptorMap = new HashMap<>();

	@Override
	public SingleValueArgumentDescriptor hasSingleValueArgument(String argumentName) {
		SingleValueArgumentDescriptor descriptor = new SingleValueArgumentDescriptorImpl(argumentName, this);
		argumentDescriptorMap.put(argumentName, descriptor);
		return descriptor;
	}

	@Override
	public MultiValueArgumentDescriptor hasMultiValueArgument(String argumentName) {
		MultiValueArgumentDescriptor descriptor = new MultiValueArgumentDescriptorImpl(argumentName, this);
		argumentDescriptorMap.put(argumentName, descriptor);
		return descriptor;
	}

	public Optional<ArgumentDescriptor<?>> getArgumentDescriptor(String argumentName) {
		ArgumentDescriptor<?> argumentDescriptor = argumentDescriptorMap.get(argumentName);
		if (argumentDescriptor == null) {
			return Optional.empty();
		}

		return Optional.of(argumentDescriptor);
	}

	public Map<String, ArgumentDescriptor<?>> getArgumentDescriptors() {
		return argumentDescriptorMap;
	}
}
