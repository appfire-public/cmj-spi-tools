package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.crowd.embedded.api.Group;

public class GroupArgumentTransformer extends AbstractArgumentTransformer<Group> implements ArgumentTransformer {

	public GroupArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected Group findReferenceById(String value) {
		return jiraManagersService.getGroupManager().getGroup(value);
	}

	@Override
	protected Group findReferenceByName(String value) {
		return jiraManagersService.getGroupManager().getGroup(value);
	}

	@Override
	protected void collectReference(String key, Group reference, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectGroup(key, reference);
	}

	@Override
	protected String getReferenceId(Group group) {
		return group.getName();
	}

	@Override
	protected String getReferenceName(Group group) {
		return group.getName();
	}

	@Override
	protected Optional<Group> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupGroup(key);
	}
}