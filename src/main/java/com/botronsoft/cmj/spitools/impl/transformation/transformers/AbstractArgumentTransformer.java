package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Collection;
import java.util.Optional;

import org.apache.log4j.Logger;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;

public abstract class AbstractArgumentTransformer<T> implements ArgumentTransformer {

	private static final Logger log = Logger.getLogger(AbstractArgumentTransformer.class);

	protected final JiraManagersService jiraManagersService;

	protected AbstractArgumentTransformer(JiraManagersService jiraManagersService) {
		this.jiraManagersService = jiraManagersService;
	}

	@Override
	public final String transformArgumentForExport(String value, Configuration configuration,
			ConfigurationReferenceCollector referenceCollector) {
		Optional<T> reference = findReference(value, configuration);
		if (!reference.isPresent()) {
			log.debug(String.format("Unable to extract reference from value '%s' for argument '%s'",
					configuration.getConfigurationPropertyName(), value));
		} else {
			collectReference(value, reference.get(), referenceCollector);
			log.debug(String.format("Successfully extracted reference from value '%s' for argument '%s'",
					configuration.getConfigurationPropertyName(), value));
		}
		return value;
	}

	@Override
	public final String transformArgumentForImport(String value, Configuration configuration,
			ConfigurationReferenceLookup referenceLookup) {
		Optional<T> resolvedReference = lookupReference(value, referenceLookup);
		if (!resolvedReference.isPresent()) {
			log.debug(String.format("Unable to resolve reference from value '%s' for argument '%s'", value,
					configuration.getConfigurationPropertyName()));
			return value;
		}

		log.debug(String.format("Successfully resolved reference from value '%s' for argument '%s'", value,
				configuration.getConfigurationPropertyName()));

		return getValueFromReference(configuration, resolvedReference.get());
	}

	private Optional<T> findReference(String value, Configuration configuration) {
		switch (configuration.getReferenceType()) {
		case BY_ID:
			return Optional.ofNullable(findReferenceById(value));
		case BY_NAME:
			return Optional.ofNullable(findReferenceByName(value));
		case BY_KEY:
			return Optional.ofNullable(findReferenceByKey(value));
		default:
			throw new IllegalArgumentException("Unknown reference type: " + configuration.getReferenceType());
		}
	}

	/**
	 * Implementations should provide means for resolving a Jira object when the value is treated as an ID.
	 *
	 * @param value
	 *            the id of the object.
	 * @return the Jira object or <code>null</code> if not found. Intentionally returns <code>null</code> to keep implementations clean.
	 */
	protected abstract T findReferenceById(String value);

	/**
	 * Implementations should provide means for resolving a Jira object when the value is treated as a name.
	 *
	 * @param value
	 *            the name of the object.
	 * @return the Jira object or <code>null</code> if not found. Intentionally returns <code>null</code> to keep implementations clean.
	 */
	protected abstract T findReferenceByName(String value);

	/**
	 * Implementations should provide means for resolving a Jira object when the value is treated as a key. This is supported only for
	 * objects which have keys (e.g. users, projects).
	 *
	 * @param value
	 *            the name of the object.
	 * @return the Jira object or <code>null</code> if not found. Intentionally returns <code>null</code> to keep implementations clean.
	 */
	protected T findReferenceByKey(String value) {
		// returns the id by default, implementations may override if they support keys different than ids
		return findReferenceById(value);
	}

	/**
	 * Collects the referenced Jira object.
	 *
	 * @param key
	 *            the reference key.
	 * @param reference
	 *            the referenced Jira object.
	 * @param referenceCollector
	 *            the SPI reference collector implementation.
	 */
	protected abstract void collectReference(String key, T reference, ConfigurationReferenceCollector referenceCollector);

	private String getValueFromReference(Configuration configuration, T resolvedReference) {
		switch (configuration.getReferenceType()) {
		case BY_ID:
			return getReferenceId(resolvedReference);
		case BY_NAME:
			return getReferenceName(resolvedReference);
		case BY_KEY:
			return getReferenceKey(resolvedReference);
		default:
			throw new IllegalArgumentException("Unknown reference type: " + configuration.getReferenceType());
		}
	}

	/**
	 * Returns the ID of the referenced object.
	 *
	 * @param reference
	 *            the Jira object.
	 * @return its ID.
	 */
	protected abstract String getReferenceId(T reference);

	/**
	 * Returns the name of the referenced object.
	 *
	 * @param reference
	 *            the Jira object.
	 * @return its name.
	 */
	protected abstract String getReferenceName(T reference);

	/**
	 * Returns the key of the referenced object. This is supported only for objects which have keys (e.g. users, projects).
	 *
	 * @param reference
	 *            the Jira object.
	 * @return its key or ID.
	 */
	protected String getReferenceKey(T reference) {
		// returns the id by default, implementations may override if they support keys different than ids
		return getReferenceId(reference);
	}

	/**
	 * Resolves a reference by its key using the SPI reference lookup implementation.
	 *
	 * @param key
	 *            the lookup key.
	 * @param referenceLookup
	 *            the SPI reference lookup implementation.
	 * @return the resolved Jira object as an {@link Optional}.
	 */
	protected abstract Optional<T> lookupReference(String key, ConfigurationReferenceLookup referenceLookup);

	/**
	 * Returns the first match of all matched references
	 *
	 * @param matches
	 *            all the matches
	 * @return the first match
	 */
	protected T findFirstMatch(Collection<T> matches, String objectName, String objectType) {
		if (matches.isEmpty()) {
			return null;
		}
		if (matches.size() > 1) {
			log.debug(String.format("There is more than one reference with name %s of type '%s'.", objectName, objectType));
		}

		return matches.iterator().next();
	}
}
