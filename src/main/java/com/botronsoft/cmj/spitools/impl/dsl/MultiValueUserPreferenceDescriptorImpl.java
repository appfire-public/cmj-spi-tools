package com.botronsoft.cmj.spitools.impl.dsl;

import com.botronsoft.cmj.spitools.dsl.MultiValueUserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceContainerDescriptor;

/**
 * Class for describing list of single values user preference.
 */
public class MultiValueUserPreferenceDescriptorImpl extends AbstractUserPreferenceDescriptor<MultiValueUserPreferenceDescriptor>
		implements MultiValueUserPreferenceDescriptor {

	private String separator;

	public MultiValueUserPreferenceDescriptorImpl(String userPreferenceName, UserPreferenceContainerDescriptor container) {
		super(userPreferenceName, container);
	}

	@Override
	public MultiValueUserPreferenceDescriptor withSeparator(String separator) {
		this.separator = separator;
		return this;
	}

	public String getSeparator() {
		return separator;
	}
}
