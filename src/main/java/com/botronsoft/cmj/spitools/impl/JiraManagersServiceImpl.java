package com.botronsoft.cmj.spitools.impl;

import com.atlassian.jira.bc.project.component.ProjectComponentManager;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.config.PriorityManager;
import com.atlassian.jira.config.ResolutionManager;
import com.atlassian.jira.config.StatusManager;
import com.atlassian.jira.event.type.EventTypeManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;
import com.atlassian.jira.issue.search.SearchRequestManager;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.workflow.WorkflowManager;

/**
 * Singleton implementation of {@link JiraManagersService}.
 */
public class JiraManagersServiceImpl implements JiraManagersService {

	@Override
	public CustomFieldManager getCustomFieldManager() {
		return ComponentAccessor.getComponent(CustomFieldManager.class);
	}

	@Override
	public OptionsManager getOptionsManager() {
		return ComponentAccessor.getComponent(OptionsManager.class);
	}

	@Override
	public FieldConfigSchemeManager getFieldConfigSchemeManager() {
		return ComponentAccessor.getComponent(FieldConfigSchemeManager.class);
	}

	@Override
	public SearchRequestManager getSearchRequestManager() {
		return ComponentAccessor.getComponent(SearchRequestManager.class);
	}

	@Override
	public GroupManager getGroupManager() {
		return ComponentAccessor.getComponent(GroupManager.class);
	}

	@Override
	public IssueLinkTypeManager getIssueLinkTypeManager() {
		return ComponentAccessor.getComponent(IssueLinkTypeManager.class);
	}

	@Override
	public IssueTypeManager getIssueTypeManager() {
		return ComponentAccessor.getComponent(IssueTypeManager.class);
	}

	@Override
	public PriorityManager getPriorityManager() {
		return ComponentAccessor.getComponent(PriorityManager.class);
	}

	@Override
	public EventTypeManager getEventTypeManager() {
		return ComponentAccessor.getComponent(EventTypeManager.class);
	}

	@Override
	public ProjectManager getProjectManager() {
		return ComponentAccessor.getComponent(ProjectManager.class);
	}

	@Override
	public VersionManager getProjectVersionManager() {
		return ComponentAccessor.getComponent(VersionManager.class);
	}

	@Override
	public ProjectComponentManager getProjectComponentManager() {
		return ComponentAccessor.getComponent(ProjectComponentManager.class);
	}

	@Override
	public ProjectRoleManager getProjectRoleManager() {
		return ComponentAccessor.getComponent(ProjectRoleManager.class);
	}

	@Override
	public JqlQueryParser getJqlQueryParser() {
		return ComponentAccessor.getComponent(JqlQueryParser.class);
	}

	@Override
	public ResolutionManager getResolutionManager() {
		return ComponentAccessor.getComponent(ResolutionManager.class);
	}

	@Override
	public StatusManager getStatusManager() {
		return ComponentAccessor.getComponent(StatusManager.class);
	}

	@Override
	public UserManager getUserManager() {
		return ComponentAccessor.getComponent(UserManager.class);
	}

	@Override
	public JqlStringSupport getJqlStringSupport() {
		return ComponentAccessor.getComponent(JqlStringSupport.class);
	}

	@Override
	public IssueSecurityLevelManager getIssueSecurityLevelManager() {
		return ComponentAccessor.getComponent(IssueSecurityLevelManager.class);
	}

	@Override
	public WorkflowManager getWorkflowManager() {
		return ComponentAccessor.getComponent(WorkflowManager.class);
	}
}
