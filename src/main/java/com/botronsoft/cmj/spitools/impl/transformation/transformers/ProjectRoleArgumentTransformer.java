package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import java.util.Optional;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;

import com.atlassian.jira.security.roles.ProjectRole;

public class ProjectRoleArgumentTransformer extends AbstractArgumentTransformer<ProjectRole> implements ArgumentTransformer {

	public ProjectRoleArgumentTransformer(JiraManagersService jiraManagersService) {
		super(jiraManagersService);
	}

	@Override
	protected ProjectRole findReferenceById(String value) {
		return jiraManagersService.getProjectRoleManager().getProjectRole(Long.valueOf(value));
	}

	@Override
	protected ProjectRole findReferenceByName(String value) {
		return jiraManagersService.getProjectRoleManager().getProjectRole(value);
	}

	@Override
	protected void collectReference(String key, ProjectRole projectRole, ConfigurationReferenceCollector referenceCollector) {
		referenceCollector.collectProjectRole(key, projectRole);
	}

	@Override
	protected String getReferenceId(ProjectRole projectRole) {
		return String.valueOf(projectRole.getId());
	}

	@Override
	protected String getReferenceName(ProjectRole projectRole) {
		return projectRole.getName();
	}

	@Override
	protected Optional<ProjectRole> lookupReference(String key, ConfigurationReferenceLookup referenceLookup) {
		return referenceLookup.lookupProjectRole(key);
	}
}
