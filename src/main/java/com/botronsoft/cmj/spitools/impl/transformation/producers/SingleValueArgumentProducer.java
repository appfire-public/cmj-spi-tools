package com.botronsoft.cmj.spitools.impl.transformation.producers;

import java.util.Set;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;

public class SingleValueArgumentProducer {

	public String produce(String value, Configuration configuration) {
		return addPrefix(value, configuration);
	}

	private String addPrefix(String value, Configuration configuration) {
		String prefix = configuration.getReferenceValuePrefix();
		Set<String> literalValues = configuration.getLiteralValues();
		if ((prefix != null) && (prefix.trim().length() > 0) && !literalValues.contains(value)) {
			return prefix.trim().concat(value);
		}
		return value;
	}
}
