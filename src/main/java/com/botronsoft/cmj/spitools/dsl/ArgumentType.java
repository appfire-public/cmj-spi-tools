package com.botronsoft.cmj.spitools.dsl;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * Represents the different types of configuration elements that can be referenced in arguments.
 */
@PublicApi
public enum ArgumentType {

	/**
	 * Represents a field - can be either a system field or a custom field.
	 */
	FIELD,

	/**
	 * Represents a custom field option.
	 */
	FIELD_OPTION,

	/**
	 * Represents a custom field context.
	 */
	FIELD_CONTEXT,

	/**
	 * Represents a user.
	 */
	USER,

	/**
	 * Represents a group.
	 */
	GROUP,

	/**
	 * Represents an issue type
	 */
	ISSUE_TYPE,

	/**
	 * Represents an issue link type.
	 */
	ISSUE_LINK_TYPE,

	/**
	 * Represents a status.
	 */
	STATUS,

	/**
	 * Represents a resolution.
	 */
	RESOLUTION,

	/**
	 * Represents a priority.
	 */
	PRIORITY,

	/**
	 * Represents an event type.
	 */
	EVENT_TYPE,

	/**
	 * Represents a saved filter.
	 */
	SEARCH_REQUEST,

	/**
	 * Represents a JQL query. This type instructs the library to parse the argument value as JQL and extract all referenced configuration
	 * elements from it.
	 */
	QUERY,

	/**
	 * Represents a project.
	 */
	PROJECT,

	/**
	 * Represents a project version.
	 */
	PROJECT_VERSION,

	/**
	 * Represents a project component.
	 */
	PROJECT_COMPONENT,

	/**
	 * Represents a project role.
	 */
	PROJECT_ROLE,

	/**
	 * Represents a issue security level.
	 */
	SECURITY_LEVEL,

	/**
	 * Represents a service desk request type.
	 */
	REQUEST_TYPE,

	/**
	 * Represents a jira software sprint type.
	 */
	SPRINT,

	/**
	 * Represents a jira software board type.
	 */
	AGILE_BOARD,

	/**
	 * Represents a workflow.
	 */
	WORKFLOW
}
