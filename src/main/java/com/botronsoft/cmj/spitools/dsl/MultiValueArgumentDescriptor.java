package com.botronsoft.cmj.spitools.dsl;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * An {@link ArgumentDescriptor} extension for multi-value arguments. Multi-value arguments hold values separated by a separator which must
 * be declared using this interface.
 */
@PublicApi
public interface MultiValueArgumentDescriptor extends ArgumentDescriptor<MultiValueArgumentDescriptor> {

	/**
	 * Describes the value separator.
	 *
	 * @param separator
	 *            the value separator.
	 * @return itself.
	 */
	MultiValueArgumentDescriptor withSeparator(String separator);

}