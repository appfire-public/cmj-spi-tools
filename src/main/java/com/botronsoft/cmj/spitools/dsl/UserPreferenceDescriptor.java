package com.botronsoft.cmj.spitools.dsl;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * Base interface for all user preference descriptors. Holds the common methods for describing user preference syntax - how references to
 * configuration elements are stored in the user preferences. Instances of this interface may be created by invoking the methods of
 * {@link UserPreferenceContainerDescriptor}.
 *
 * @param <T>
 *            the actual extending interface of this base interface.
 */
@PublicApi
public interface UserPreferenceDescriptor<T extends UserPreferenceDescriptor<?>> {

	/**
	 * Describes the type of a referenced configuration element in this user preference.
	 *
	 * @param userPreferenceType
	 *            the type of a referenced configuration element in this user preference.
	 * @return itself.
	 */
	T ofType(UserPreferenceType userPreferenceType);

	/**
	 * Describes if the configuration element is referenced by id in this user preference. For {@link UserPreferenceType#USER} this method
	 * will behave in the same way as {@link #byKey()}.
	 *
	 * @return itself.
	 */
	T byId();

	/**
	 * Describes if the configuration element is referenced by name in this user preference.
	 *
	 * @return itself.
	 */
	T byName();

	/**
	 * Describes if the configuration element is referenced by key in this user preference. This is supported only for elements which have
	 * keys - {@link UserPreferenceType#PROJECT} and {@link UserPreferenceType#USER}. For all other configuration elements this method will
	 * behave in the same way as {@link #byId()}.
	 *
	 * @return itself.
	 */
	T byKey();

	/**
	 * Describes that the configuration element is referenced by value with prefix in this user preference.
	 *
	 * @param prefix
	 *            the prefix of the value
	 * @return itself.
	 */
	T withPrefix(String prefix);

	/**
	 * Describes the literal values of this user preference that should be moved as-is. If the user preference holds any of the values
	 * passed to this method, it will be exported and imported without any modifications.
	 *
	 * @param literalValues
	 *            literal values of this user preference.
	 * @return itself.
	 */
	T withLiteralValues(String... literalValues);

	/**
	 * Describes addition of a new user preference descriptor for the parent {@link UserPreferenceContainerDescriptor}.
	 *
	 * @return the parent {@link UserPreferenceContainerDescriptor} instance.
	 */
	UserPreferenceContainerDescriptor and();
}
