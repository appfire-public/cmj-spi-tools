package com.botronsoft.cmj.spitools.dsl;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * Represents the different types of configuration elements that can be referenced in user preferences.
 */
@PublicApi
public enum UserPreferenceType {

	/**
	 * Represents a field - can be either a system field or a custom field.
	 */
	FIELD,

	/**
	 * Represents a custom field option.
	 */
	FIELD_OPTION,

	/**
	 * Represents a user.
	 */
	USER,

	/**
	 * Represents a group.
	 */
	GROUP,

	/**
	 * Represents an issue type
	 */
	ISSUE_TYPE,

	/**
	 * Represents an issue link type.
	 */
	ISSUE_LINK_TYPE,

	/**
	 * Represents a status.
	 */
	STATUS,

	/**
	 * Represents a resolution.
	 */
	RESOLUTION,

	/**
	 * Represents a priority.
	 */
	PRIORITY,

	/**
	 * Represents a saved filter.
	 */
	SEARCH_REQUEST,

	/**
	 * Represents a JQL query. This type instructs the library to parse the user preference value as JQL and extract all referenced
	 * configuration elements from it.
	 */
	QUERY,

	/**
	 * Represents a project.
	 */
	PROJECT,

	/**
	 * Represents a project role.
	 */
	PROJECT_ROLE,

	/**
	 * Represents a issue security level.
	 */
	SECURITY_LEVEL,

	/**
	 * Represents a service desk request type.
	 */
	REQUEST_TYPE,

	/**
	 * Represents a jira software sprint.
	 */
	SPRINT
}
