package com.botronsoft.cmj.spitools.dsl;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * A {@link UserPreferenceDescriptor} extension for single-value user preferences.
 */
@PublicApi
public interface SingleValueUserPreferenceDescriptor extends UserPreferenceDescriptor<SingleValueUserPreferenceDescriptor> {

}
