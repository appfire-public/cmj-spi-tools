package com.botronsoft.cmj.spitools.dsl;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * Base interface for all argument descriptors. Holds the common methods for describing argument syntax - how references to configuration
 * elements are stored in the arguments. Instances of this interface may be created by invoking the methods of
 * {@link ArgumentContainerDescriptor}.
 *
 * @param <T>
 *            the actual extending interface of this base interface.
 */
@PublicApi
public interface ArgumentDescriptor<T extends ArgumentDescriptor<?>> {

	/**
	 * Describes the type of a referenced configuration element in this argument.
	 *
	 * @param argumentType
	 *            type of a referenced configuration element in this argument.
	 * @return itself.
	 */
	T ofType(ArgumentType argumentType);

	/**
	 * Describes if the configuration element is referenced by id in this argument. For {@link ArgumentType#USER} this method will behave in
	 * the same way as {@link #byKey()}.
	 *
	 * @return itself.
	 */
	T byId();

	/**
	 * Describes if the configuration element is referenced by name in this argument.
	 *
	 * @return itself.
	 */
	T byName();

	/**
	 * Describes if the configuration element is referenced by key in this argument. This is supported only for elements which have keys -
	 * {@link ArgumentType#PROJECT} and {@link ArgumentType#USER}. For all other configuration elements this method will behave in the same
	 * way as {@link #byId()}.
	 *
	 * @return itself.
	 */
	T byKey();

	/**
	 * Describes that the configuration element is referenced by value with prefix.
	 *
	 * @param prefix
	 *            the prefix of the value
	 * @return itself.
	 */
	T withPrefix(String prefix);

	/**
	 * Describes the literal values of this argument that should be moved as-is. If the argument holds any of the values passed to this
	 * method, it will be exported and imported without any modifications.
	 *
	 * @param literalValues
	 *            literal values of this argument.
	 * @return itself.
	 */
	T withLiteralValues(String... literalValues);

	/**
	 * Describes addition of a new argument descriptor for the parent {@link ArgumentContainerDescriptor}.
	 *
	 * @return the parent {@link ArgumentContainerDescriptor} instance.
	 */
	ArgumentContainerDescriptor and();
}
