package com.botronsoft.cmj.spitools.dsl;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * The root of the argument descriptor DSL. Can be used to describe single-value and multi-value arguments which reference configuration
 * elements of different types (e.g. fields, statuses, project roles, etc.).
 */
@PublicApi
public interface ArgumentContainerDescriptor {

	/**
	 * Returns a {@link SingleValueArgumentDescriptor} instance which can be used to describe an argument which holds only a single value.
	 *
	 * @param argumentName
	 *            the name of the argument which is being described.
	 * @return a {@link SingleValueArgumentDescriptor} instance.
	 */
	SingleValueArgumentDescriptor hasSingleValueArgument(String argumentName);

	/**
	 * Returns a {@link MultiValueArgumentDescriptor} instance which can be used to describe an argument which holds multiple values,
	 * separated with a separator like comma, colon, etc.
	 *
	 * @param argumentName
	 *            the name of the argument which is being described.
	 * @return a {@link MultiValueArgumentDescriptor} instance.
	 */
	MultiValueArgumentDescriptor hasMultiValueArgument(String argumentName);
}
