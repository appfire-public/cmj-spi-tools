package com.botronsoft.cmj.spitools.dsl;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * An {@link ArgumentDescriptor} extension for single-value arguments.
 */
@PublicApi
public interface SingleValueArgumentDescriptor extends ArgumentDescriptor<SingleValueArgumentDescriptor> {

}
