package com.botronsoft.cmj.spitools.dsl;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * A {@link UserPreferenceDescriptor} extension for multi-value user preferences. Multi-value user preferences hold values separated by a
 * separator which must be declared using this interface.
 */
@PublicApi
public interface MultiValueUserPreferenceDescriptor extends UserPreferenceDescriptor<MultiValueUserPreferenceDescriptor> {

	/**
	 * Describes the value separator.
	 *
	 * @param separator
	 *            the value separator.
	 * @return itself.
	 */
	MultiValueUserPreferenceDescriptor withSeparator(String separator);

}