package com.botronsoft.cmj.spitools.dsl;

import com.botronsoft.cmj.spi.annotations.PublicApi;

/**
 * The root of the user preference descriptor DSL. Can be used to describe single-value and multi-value user preferences which reference
 * configuration elements of different types (e.g. fields, statuses, project roles, etc.).
 */
@PublicApi
public interface UserPreferenceContainerDescriptor {

	/**
	 * Returns a {@link SingleValueUserPreferenceDescriptor} instance which can be used to describe an user preference which holds only a
	 * single value.
	 *
	 * @param userPreferenceName
	 *            the name of the user preference which is being described.
	 * @return a {@link SingleValueUserPreferenceDescriptor} instance.
	 */
	SingleValueUserPreferenceDescriptor hasSingleValueUserPreference(String userPreferenceName);

	/**
	 * Returns a {@link MultiValueUserPreferenceDescriptor} instance which can be used to describe an user preference which holds multiple
	 * values, separated with a separator like comma, colon, etc.
	 *
	 * @param userPreferenceName
	 *            the name of the user preference which is being described.
	 * @return a {@link MultiValueUserPreferenceDescriptor} instance.
	 */
	MultiValueUserPreferenceDescriptor hasMultiValueUserPreference(String userPreferenceName);
}
