package com.botronsoft.cmj.spitools.gadget;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.log4j.Logger;

import com.botronsoft.cmj.spi.annotations.PublicApi;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;
import com.botronsoft.cmj.spi.configuration.gadget.DashboardGadgetHandler;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceContainerDescriptor;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceType;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;
import com.botronsoft.cmj.spitools.impl.JiraManagersServiceImpl;
import com.botronsoft.cmj.spitools.impl.dsl.AbstractUserPreferenceDescriptor;
import com.botronsoft.cmj.spitools.impl.dsl.ReferenceValueType;
import com.botronsoft.cmj.spitools.impl.dsl.UserPreferenceContainerDescriptorImpl;
import com.botronsoft.cmj.spitools.impl.transformation.ArgumentProcessor;
import com.botronsoft.cmj.spitools.impl.transformation.ArgumentProcessorFactory;

/**
 * Base class for {@link DashboardGadgetHandler} implementations that want to use the DSL for describing the syntax of gadget user
 * preferences. Extend this class and implement the {@link #describeUserPreferences()} method to describe the syntax of all user preferences
 * by invoking the {@link #gadget(String)} method.
 */
@PublicApi
public abstract class AbstractDashboardGadgetHandler implements DashboardGadgetHandler {

	private static final Logger log = Logger.getLogger(AbstractDashboardGadgetHandler.class);

	private final ArgumentProcessorFactory argumentProcessorFactory;
	private final Map<String, UserPreferenceContainerDescriptor> userPreferenceContainerDescriptorsMap = new HashMap<>();

	/**
	 * Non-argument constructor for implementations.
	 */
	public AbstractDashboardGadgetHandler() {
		this(new JiraManagersServiceImpl());
	}

	AbstractDashboardGadgetHandler(JiraManagersService jiraManagersService) {
		this.argumentProcessorFactory = new ArgumentProcessorFactory(jiraManagersService);
		describeUserPreferences();
		validateDescriptors();
	}

	@Override
	public Map<String, String> transformUserPreferencesForExport(String uriOrModuleKey, Map<String, String> userPreferences,
			ExportContext exportContext) {
		return transformUserPreferences(uriOrModuleKey, userPreferences,
				(value, argumentProcessor) -> argumentProcessor.processArgumentForExport(value, exportContext));
	}

	@Override
	public Map<String, String> transformUserPreferencesForImport(String uriOrModuleKey, Map<String, String> userPreferences,
			ImportContext importContext) {
		return transformUserPreferences(uriOrModuleKey, userPreferences,
				(value, argumentProcessor) -> argumentProcessor.processArgumentForImport(value, importContext));
	}

	/**
	 * Implement this method to describe the syntax of gadget user preferences.
	 *
	 * @see {@link #gadget(String)}.
	 */
	protected abstract void describeUserPreferences();

	/**
	 * This method will be invoked when an exception is caught during processing of a user preference. The default implementation is to log
	 * an error - in this case the original value will be preserved.
	 *
	 * @param uriOrModuleKey
	 *            the URI of the dashboard gadget or the module key of the dashboard item.
	 * @param userPreferenceName
	 *            the name of the user preference that failed to export.
	 * @param t
	 *            thrown on error.
	 */
	protected void handleError(String uriOrModuleKey, String userPreferenceName, Throwable t) {
		log.error("Failed to transform user preference with name " + userPreferenceName + " belonging to gadget " + uriOrModuleKey + "!",
				t);
	}

	/**
	 * This method will be invoked for each user preference that has not been described in {@link #describeUserPreferences()}. The default
	 * behavior is to return the same value.
	 *
	 * @param uriOrModuleKey
	 *            the URI of the dashboard gadget or the module key of the dashboard item.
	 * @param userPreferenceName
	 *            the name of the user preference.
	 * @param userPreferenceValue
	 *            the raw value of the user preference.
	 * @param userPreferences
	 *            all other raw user preferences being exported.
	 * @param exportContext
	 *            the context of the export operation.
	 * @return the transformed value of the user preference.
	 */
	protected String transformUnhandledUserPreferencesForExport(String uriOrModuleKey, String userPreferenceName,
			String userPreferenceValue, Map<String, String> userPreferences, ExportContext exportContext) {
		// default behavior is to return the same user preference value
		return userPreferenceValue;
	}

	/**
	 * This method will be invoked for each user preference that has not been described in {@link #describeUserPreferences()}. The default
	 * behavior is to return the same value.
	 *
	 * @param uriOrModuleKey
	 *            the URI of the dashboard gadget or the module key of the dashboard item.
	 * @param userPreferenceName
	 *            the name of the user preference.
	 * @param userPreferenceValue
	 *            the raw value of the user preference.
	 * @param userPreferences
	 *            all other raw user preferences being imported.
	 * @param importContext
	 *            the context of the import operation.
	 * @return the transformed value of the user preference that will be effectively stored for the corresponding gadget.
	 */
	protected String transformUnhandledUserPreferencesForImport(String uriOrModuleKey, String userPreferenceName,
			String userPreferenceValue, Map<String, String> userPreferences, ImportContext importContext) {
		// default behavior is to return the same user preference value
		return userPreferenceValue;
	}

	/**
	 * Describes the syntax of the different user preferences for a gadget.
	 *
	 * @param uriOrModuleKey
	 *            the URI of the dashboard gadget or the module key of the dashboard item. This is the same URI that is used in the
	 *            atlassian-plugin.xml descriptor. See the Dashboards SPI public documentation for instructions on how to get the URI for
	 *            your gadget.
	 * @return the descriptor which can be used to further describe the user preferences.
	 */
	protected final UserPreferenceContainerDescriptor gadget(String uriOrModuleKey) {
		return getOrCreateUserPreferenceDescriptor(uriOrModuleKey);
	}

	private void validateDescriptors() {
		for (Entry<String, UserPreferenceContainerDescriptor> descriptorEntry : userPreferenceContainerDescriptorsMap.entrySet()) {
			Map<String, UserPreferenceDescriptor<?>> userPreferenceDescriptors = ((UserPreferenceContainerDescriptorImpl) descriptorEntry
					.getValue()).getUserPreferenceDescriptors();
			Iterator<Entry<String, UserPreferenceDescriptor<?>>> iterator = userPreferenceDescriptors.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, UserPreferenceDescriptor<?>> userPreferenceEntry = iterator.next();
				AbstractUserPreferenceDescriptor descriptor = ((AbstractUserPreferenceDescriptor) userPreferenceEntry.getValue());
				if (descriptor.getUserPreferenceType().equals(UserPreferenceType.REQUEST_TYPE)
						&& descriptor.getReferenceValueType().equals(ReferenceValueType.NAME)) {
					log.warn("Request types can only be referred by id.");
					iterator.remove();
				}
				if (descriptor.getUserPreferenceType().equals(UserPreferenceType.SPRINT)
						&& descriptor.getReferenceValueType().equals(ReferenceValueType.NAME)) {
					log.warn("Sprints can only be referred by id.");
					iterator.remove();
				}

			}
		}
	}

	private UserPreferenceContainerDescriptor getOrCreateUserPreferenceDescriptor(String uriOrModuleKey) {
		UserPreferenceContainerDescriptor userPreferenceContainerDescriptor = userPreferenceContainerDescriptorsMap.get(uriOrModuleKey);
		if (userPreferenceContainerDescriptor == null) {
			userPreferenceContainerDescriptor = new UserPreferenceContainerDescriptorImpl();
			userPreferenceContainerDescriptorsMap.put(uriOrModuleKey, userPreferenceContainerDescriptor);
		}

		return userPreferenceContainerDescriptor;
	}

	private Map<String, String> transformUserPreferences(String uriOrModuleKey, Map<String, String> userPreferences,
			ArgumentProcessorInvoker argumentProcessorInvoker) {
		Map<String, String> transformedArgs = new LinkedHashMap<>();

		for (Entry<String, String> entry : userPreferences.entrySet()) {
			try {
				ArgumentProcessor argumentProcessor = createArgumentProcessor(uriOrModuleKey, entry.getKey(),
						Collections.unmodifiableMap(userPreferences));
				String transformedValue = argumentProcessorInvoker.invoke(entry.getValue(), argumentProcessor);

				transformedArgs.put(entry.getKey(), transformedValue);
			} catch (Throwable t) {
				handleError(uriOrModuleKey, entry.getKey(), t);
				transformedArgs.put(entry.getKey(), entry.getValue());
			}
		}

		return transformedArgs;

	}

	private ArgumentProcessor createArgumentProcessor(String uriOrModuleKey, String userPreferenceName,
			Map<String, String> userPreferences) {
		UserPreferenceContainerDescriptor userPreferenceContainerDescriptor = userPreferenceContainerDescriptorsMap.get(uriOrModuleKey);
		if (userPreferenceContainerDescriptor != null) {
			Optional<UserPreferenceDescriptor<?>> userPreferenceDescriptorOptional = ((UserPreferenceContainerDescriptorImpl) userPreferenceContainerDescriptor)
					.getUserPreferenceDescriptor(userPreferenceName);
			if (userPreferenceDescriptorOptional.isPresent()) {
				return argumentProcessorFactory.createProcessor(userPreferenceDescriptorOptional.get());
			}
		}

		return new DefaultArgumentProcessor(uriOrModuleKey, userPreferenceName, userPreferences);
	}

	private interface ArgumentProcessorInvoker {
		String invoke(String userPreferenceName, ArgumentProcessor argumentProcessor);
	}

	private final class DefaultArgumentProcessor implements ArgumentProcessor {

		private final String uriOrModuleKey;
		private final String userPreferenceName;
		private final Map<String, String> userPreferences;

		private DefaultArgumentProcessor(String uriOrModuleKey, String userPreferenceName, Map<String, String> userPreferences) {
			this.uriOrModuleKey = uriOrModuleKey;
			this.userPreferenceName = userPreferenceName;
			this.userPreferences = userPreferences;
		}

		@Override
		public String processArgumentForExport(String value, ExportContext exportContext) {
			return transformUnhandledUserPreferencesForExport(uriOrModuleKey, userPreferenceName, value, userPreferences, exportContext);
		}

		@Override
		public String processArgumentForImport(String value, ImportContext importContext) {
			return transformUnhandledUserPreferencesForImport(uriOrModuleKey, userPreferenceName, value, userPreferences, importContext);
		}

	}
}
