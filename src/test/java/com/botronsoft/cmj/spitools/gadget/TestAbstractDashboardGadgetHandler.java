package com.botronsoft.cmj.spitools.gadget;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;
import com.botronsoft.cmj.spi.configuration.gadget.DashboardGadgetHandler;
import com.botronsoft.cmj.spitools.dsl.UserPreferenceType;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;
import com.google.common.collect.Lists;

import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.issue.issuetype.IssueType;

@RunWith(MockitoJUnitRunner.class)
public class TestAbstractDashboardGadgetHandler {

	@Mock
	private ExportContext mockExportContext;

	@Mock
	private ImportContext mockImportContext;

	@Mock
	private ConfigurationReferenceCollector mockReferenceCollector;

	@Mock
	private ConfigurationReferenceLookup mockReferenceLookup;

	@Mock
	private JiraManagersService mockJiraManagersService;

	@Mock
	private IssueTypeManager mockIssueTypeManager;

	@Mock
	private IssueType mockIssueType1;

	@Mock
	private IssueType mockIssueType2;

	@Before
	public void setUp() {
		when(mockExportContext.getReferenceCollector()).thenReturn(mockReferenceCollector);
		when(mockImportContext.getReferenceLookup()).thenReturn(mockReferenceLookup);
	}

	@Test
	public void exportSingleValueUserPreference() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10003")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueTypes()).thenReturn(Lists.newArrayList(mockIssueType1, mockIssueType2));
		when(mockIssueType1.getName()).thenReturn("User Story");
		when(mockIssueType2.getName()).thenReturn("Bug");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasSingleValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byName().and().
						hasSingleValueUserPreference("arg2").ofType(UserPreferenceType.ISSUE_TYPE).byId();
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "10003");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("some.gadget", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug"));
		assertThat(outputMap, hasEntry("arg2", "10003"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10003", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("Bug", mockIssueType2);
	}

	@Test
	public void exportSingleValueUserPreferenceWithPrefix() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10003")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueTypes()).thenReturn(Lists.newArrayList(mockIssueType1, mockIssueType2));
		when(mockIssueType1.getName()).thenReturn("User Story");
		when(mockIssueType2.getName()).thenReturn("Bug");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasSingleValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byName().withPrefix("prefixName-").and().
						hasSingleValueUserPreference("arg2").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix("prefixId-");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "prefixName-Bug");
		inputMap.put("arg2", "prefixId-10003");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("some.gadget", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug"));
		assertThat(outputMap, hasEntry("arg2", "10003"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10003", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("Bug", mockIssueType2);
	}

	@Test
	public void exportSingleValueUserPreferenceWithNullableOrEmptyPrefix() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10003")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueTypes()).thenReturn(Lists.newArrayList(mockIssueType1, mockIssueType2));
		when(mockIssueType1.getName()).thenReturn("User Story");
		when(mockIssueType2.getName()).thenReturn("Bug");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasSingleValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byName().withPrefix("  ").and().
						hasSingleValueUserPreference("arg2").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix(null);
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "10003");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("some.gadget", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug"));
		assertThat(outputMap, hasEntry("arg2", "10003"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10003", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("Bug", mockIssueType2);
	}

	@Test
	public void exportMultiValueUserPreference() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10005")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueType("10001")).thenReturn(mockIssueType2);

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("some.gadget", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10005,10001"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10005", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("10001", mockIssueType2);
	}

	@Test
	public void exportMultiValueUserPreferenceWithPrefix() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10005")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueType("10001")).thenReturn(mockIssueType2);

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix("prefixId-").withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "prefixId-10005,prefixId-10001");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("some.gadget", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10005,10001"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10005", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("10001", mockIssueType2);
	}

	@Test
	public void exportMultiValueUserPreferenceWithNullablePrefix() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10005")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueType("10001")).thenReturn(mockIssueType2);

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix(null).withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("some.gadget", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10005,10001"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10005", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("10001", mockIssueType2);
	}

	@Test
	public void exportMultiValueUserPreferenceWithEmptyPrefix() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10005")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueType("10001")).thenReturn(mockIssueType2);

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix("  ").withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("some.gadget", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10005,10001"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10005", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("10001", mockIssueType2);
	}

	@Test
	public void exportSingleValueUserPreferenceLiteralValue() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueTypes()).thenReturn(Lists.newArrayList(mockIssueType1, mockIssueType2));
		when(mockIssueType1.getName()).thenReturn("User Story");
		when(mockIssueType2.getName()).thenReturn("Bug");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasSingleValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byName().and().
						hasSingleValueUserPreference("arg2").ofType(UserPreferenceType.ISSUE_TYPE).byId().withLiteralValues("none", "maybe");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "none");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("some.gadget", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug"));
		assertThat(outputMap, hasEntry("arg2", "none"));

		verify(mockReferenceCollector, times(1)).collectIssueType("Bug", mockIssueType2);
	}

	@Test
	public void exportMultiValueUserPreferenceLiteral() {
		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withSeparator(",").withLiteralValues("none");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "none");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("some.gadget", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "none"));
	}

	@Test
	public void userPreferencesValidation() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10003")).thenReturn(mockIssueType1);

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasSingleValueUserPreference("arg1").ofType(UserPreferenceType.REQUEST_TYPE).byName().and().
						hasSingleValueUserPreference("arg2").ofType(UserPreferenceType.ISSUE_TYPE).byId().and().
						hasSingleValueUserPreference("arg3").ofType(UserPreferenceType.SPRINT).byName();
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "5");
		inputMap.put("arg2", "10003");
		inputMap.put("arg3", "10001");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("some.gadget", inputMap, mockExportContext);

		assertThat(outputMap, equalTo(inputMap));

		verify(mockReferenceCollector, times(1)).collectIssueType("10003", mockIssueType1);
		verify(mockReferenceCollector, times(0)).collectRequestType("5", 5L);
		verify(mockReferenceCollector, times(0)).collectSprint("1001", 1001L);
	}

	@Test
	public void importSingleValueUserPreference() {
		when(mockReferenceLookup.lookupIssueType("Bug")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10003")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getName()).thenReturn("Bug2");
		when(mockIssueType2.getId()).thenReturn("10005");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasSingleValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byName().and().
						hasSingleValueUserPreference("arg2").ofType(UserPreferenceType.ISSUE_TYPE).byId();
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "10003");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug2"));
		assertThat(outputMap, hasEntry("arg2", "10005"));
	}

	@Test
	public void importSingleValueUserPreferenceWithPrefix() {
		when(mockReferenceLookup.lookupIssueType("prefixName-Bug")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("prefixId-10003")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getName()).thenReturn("Bug2");
		when(mockIssueType2.getId()).thenReturn("10005");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasSingleValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byName().withPrefix("prefixName-").and().
						hasSingleValueUserPreference("arg2").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix("prefixId-");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "prefixName-Bug");
		inputMap.put("arg2", "prefixId-10003");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "prefixName-Bug2"));
		assertThat(outputMap, hasEntry("arg2", "prefixId-10005"));
	}

	@Test
	public void importSingleValueUserPreferenceWithNullableOrEmptyPrefix() {
		when(mockReferenceLookup.lookupIssueType("Bug")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10003")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getName()).thenReturn("Bug2");
		when(mockIssueType2.getId()).thenReturn("10005");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasSingleValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byName().withPrefix(null).and().
						hasSingleValueUserPreference("arg2").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix("    ");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "10003");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug2"));
		assertThat(outputMap, hasEntry("arg2", "10005"));
	}

	@Test
	public void importMultiValueUserPreference() {
		when(mockReferenceLookup.lookupIssueType("10005")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10001")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getId()).thenReturn("10015");
		when(mockIssueType2.getId()).thenReturn("10011");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10015,10011"));
	}

	@Test
	public void importMultiValueUserPreferenceWithPrefix() {
		when(mockReferenceLookup.lookupIssueType("10005")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10001")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getId()).thenReturn("10015");
		when(mockIssueType2.getId()).thenReturn("10011");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix("prefixId-").withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "prefixId-10005,prefixId-10001");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "prefixId-10015,prefixId-10011"));
	}

	@Test
	public void importMultiValueUserPreferenceWithNullablePrefix() {
		when(mockReferenceLookup.lookupIssueType("10005")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10001")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getId()).thenReturn("10015");
		when(mockIssueType2.getId()).thenReturn("10011");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix(null).withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10015,10011"));
	}

	@Test
	public void importMultiValueUserPreferenceWithEmptyPrefix() {
		when(mockReferenceLookup.lookupIssueType("10005")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10001")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getId()).thenReturn("10015");
		when(mockIssueType2.getId()).thenReturn("10011");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix("  ").withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10015,10011"));
	}

	@Test
	public void importSingleValueUserPreferenceLiteral() {
		when(mockReferenceLookup.lookupIssueType("Bug")).thenReturn(Optional.of(mockIssueType1));
		when(mockIssueType1.getName()).thenReturn("Bug2");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasSingleValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byName().and().
						hasSingleValueUserPreference("arg2").ofType(UserPreferenceType.ISSUE_TYPE).byId().withLiteralValues("none");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "none");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug2"));
		assertThat(outputMap, hasEntry("arg2", "none"));
	}

	@Test
	public void importSingleValueUserPreferenceLiteralWithPrefix() {
		when(mockReferenceLookup.lookupIssueType("Bug")).thenReturn(Optional.of(mockIssueType1));
		when(mockIssueType1.getName()).thenReturn("Bug2");

		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasSingleValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byName().and().
						hasSingleValueUserPreference("arg2").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix("prefix-").withLiteralValues("none");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "none");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug2"));
		assertThat(outputMap, hasEntry("arg2", "none"));
	}

	@Test
	public void importMultiValueUserPreferenceLiteral() {
		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withSeparator(",").withLiteralValues("none");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "none");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "none"));
	}

	@Test
	public void importMultiValueUserPreferenceLiteralAndPrefix() {
		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {

			@Override
			protected void describeUserPreferences() {
				//@formatter:off
				gadget("some.gadget").
						hasMultiValueUserPreference("arg1").ofType(UserPreferenceType.ISSUE_TYPE).byId().withPrefix("prefix-").withSeparator(",").withLiteralValues("none", "any");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "none");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("some.gadget", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "none"));
	}

	@Test
	public void unhandledUserPreferenceOnExport() {
		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {
			@Override
			protected void describeUserPreferences() {

			}
		};

		Map<String, String> inputMap = Collections.singletonMap("my.prop", "value 1");

		Map<String, String> outputMap = handler.transformUserPreferencesForExport("participant", inputMap, mockExportContext);
		assertThat(outputMap, equalTo(inputMap));
	}

	@Test
	public void unhandledUserPreferenceOnImport() {
		DashboardGadgetHandler handler = new ErrorThrowingDashboardGadgetHandler(mockJiraManagersService) {
			@Override
			protected void describeUserPreferences() {

			}
		};

		Map<String, String> inputMap = Collections.singletonMap("my.prop", "value 1");

		Map<String, String> outputMap = handler.transformUserPreferencesForImport("participant", inputMap, mockImportContext);
		assertThat(outputMap, equalTo(inputMap));
	}

	private static abstract class ErrorThrowingDashboardGadgetHandler extends AbstractDashboardGadgetHandler {

		public ErrorThrowingDashboardGadgetHandler(JiraManagersService jiraManagersService) {
			super(jiraManagersService);
		}

		@Override
		protected void handleError(String uriOrModuleKey, String userPreferenceName, Throwable t) {
			throw new IllegalStateException(t);
		}
	}

}
