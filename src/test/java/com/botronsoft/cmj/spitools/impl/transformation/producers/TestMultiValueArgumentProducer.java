package com.botronsoft.cmj.spitools.impl.transformation.producers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

@RunWith(MockitoJUnitRunner.class)
public class TestMultiValueArgumentProducer {

	@Mock
	private Configuration mockConfiguration;

	private MultiValueArgumentProducer producer;

	@Before
	public void setUp() {
		producer = new MultiValueArgumentProducer();

		when(mockConfiguration.getLiteralValues()).thenReturn(Sets.newHashSet("none", "any"));
		when(mockConfiguration.getSeparator()).thenReturn(Optional.of(","));
	}

	@Test
	public void produceValueWithPrefix() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("prefix-");
		List<String> values = Lists.newArrayList("field1", "field2", "field3");
		String result = producer.produce(values, mockConfiguration);
		assertThat(result, is("prefix-field1,prefix-field2,prefix-field3"));
	}

	@Test
	public void produceValueWithEmptyPrefix() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("  ");
		List<String> values = Lists.newArrayList("field1", "field2", "field3");
		String result = producer.produce(values, mockConfiguration);
		assertThat(result, is("field1,field2,field3"));
	}

	@Test
	public void produceEmptyValueWithPrefix() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("prefix-");
		List<String> values = Lists.newArrayList("");
		String result = producer.produce(values, mockConfiguration);
		assertThat(result, is("prefix-"));
	}

	@Test
	public void produceValueInLiteralValues() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("prefix-");
		List<String> values = Lists.newArrayList("none");
		String result = producer.produce(values, mockConfiguration);
		assertThat(result, is("none"));
	}

}
