package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.config.ResolutionManager;
import com.atlassian.jira.issue.resolution.Resolution;

@RunWith(MockitoJUnitRunner.class)
public class TestResolutionArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	ResolutionManager mockResolutionManager;

	@Mock
	private Resolution mockResolution;

	private ResolutionArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new ResolutionArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformResolutionArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getResolutionManager()).thenReturn(mockResolutionManager);
		when(mockResolutionManager.getResolution("10005")).thenReturn(mockResolution);

		String transformedValue = transformer.transformArgumentForExport("10005", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectResolution("10005", mockResolution);
		assertThat(transformedValue, is("10005"));
	}

	@Test
	public void transformResolutionArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getResolutionManager()).thenReturn(mockResolutionManager);
		when(mockResolutionManager.getResolutionByName("Duplicate")).thenReturn(mockResolution);

		String transformedValue = transformer.transformArgumentForExport("Duplicate", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectResolution("Duplicate", mockResolution);
		assertThat(transformedValue, is("Duplicate"));
	}

	@Test
	public void transformResolutionArgumentForExportWithNullObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getResolutionManager()).thenReturn(mockResolutionManager);
		when(mockResolutionManager.getResolution("10005")).thenReturn(null);

		transformer.transformArgumentForExport("10005", mockConfiguration, mockReferenceCollector);

	}

	@Test
	public void transformResolutionArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupResolution("10005")).thenReturn(Optional.of(mockResolution));
		when(mockResolution.getId()).thenReturn("10006");

		String transformedValue = transformer.transformArgumentForImport("10005", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("10006"));
	}

	@Test
	public void transformResolutionArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupResolution("Duplicate")).thenReturn(Optional.of(mockResolution));
		when(mockResolution.getName()).thenReturn("duplicate2");

		String transformedValue = transformer.transformArgumentForImport("Duplicate", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("duplicate2"));
	}

	@Test
	public void transformResolutionArgumentForImportWithNullObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupResolution("10005")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("10005", mockConfiguration, mockReferenceLookup);
	}
}
