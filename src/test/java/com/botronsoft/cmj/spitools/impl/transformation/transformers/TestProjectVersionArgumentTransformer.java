package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectVersionArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	VersionManager mockVersionManager;

	@Mock
	private Version mockVersion;

	private ProjectVersionArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new ProjectVersionArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformIssueTypeArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getProjectVersionManager()).thenReturn(mockVersionManager);
		when(mockVersionManager.getVersion(5l)).thenReturn(mockVersion);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectProjectVersion("5", mockVersion);
		assertThat(transformedValue, is("5"));
	}

	@Test
	public void transformIssueTypeArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getProjectVersionManager()).thenReturn(mockVersionManager);
		when(mockVersionManager.getVersionsByName("versionName")).thenReturn(Collections.singletonList(mockVersion));
		when(mockVersion.getName()).thenReturn("versionName");

		String transformedValue = transformer.transformArgumentForExport("versionName", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectProjectVersion("versionName", mockVersion);
		assertThat(transformedValue, is("versionName"));
	}

	@Test
	public void transformIssueTypeArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getProjectVersionManager()).thenReturn(mockVersionManager);
		when(mockVersionManager.getVersion(5l)).thenReturn(null);

		transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformIssueTypeArgumentForExportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getProjectVersionManager()).thenReturn(mockVersionManager);
		when(mockVersionManager.getVersionsByName("versionName")).thenReturn(Collections.emptyList());

		transformer.transformArgumentForExport("versionName", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformIssueTypeArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupProjectVersion("5")).thenReturn(Optional.of(mockVersion));
		when(mockVersion.getId()).thenReturn(6l);

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}

	@Test
	public void transformIssueTypeArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupProjectVersion("versionName")).thenReturn(Optional.of(mockVersion));
		when(mockVersion.getName()).thenReturn("versionName2");

		String transformedValue = transformer.transformArgumentForImport("versionName", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("versionName2"));
	}

	@Test
	public void transformIssueTypeArgumentForImportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupProjectVersion("versionName")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("versionName", mockConfiguration, mockReferenceLookup);
	}
}
