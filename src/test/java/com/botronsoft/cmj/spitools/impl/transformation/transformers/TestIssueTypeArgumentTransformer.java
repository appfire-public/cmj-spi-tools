package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.issue.issuetype.IssueType;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueTypeArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	IssueTypeManager mockIssueTypeManager;

	@Mock
	private IssueType mockIssueType;

	private IssueTypeArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new IssueTypeArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformIssueTypeArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("5")).thenReturn(mockIssueType);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectIssueType("5", mockIssueType);
		assertThat(transformedValue, is("5"));
	}

	@Test
	public void transformIssueTypeArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueTypes()).thenReturn(Collections.singletonList(mockIssueType));
		when(mockIssueType.getName()).thenReturn("issueTypeName");

		String transformedValue = transformer.transformArgumentForExport("issueTypeName", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectIssueType("issueTypeName", mockIssueType);
		assertThat(transformedValue, is("issueTypeName"));
	}

	@Test
	public void transformIssueTypeArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("5")).thenReturn(null);

		transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformIssueTypeArgumentForExportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueTypes()).thenReturn(Collections.emptyList());

		transformer.transformArgumentForExport("issueTypeName", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformIssueTypeArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupIssueType("5")).thenReturn(Optional.of(mockIssueType));
		when(mockIssueType.getId()).thenReturn("6");

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}

	@Test
	public void transformIssueTypeArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupIssueType("issueTypeName")).thenReturn(Optional.of(mockIssueType));
		when(mockIssueType.getName()).thenReturn("issueTypeName2");

		String transformedValue = transformer.transformArgumentForImport("issueTypeName", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("issueTypeName2"));
	}

	@Test
	public void transformIssueTypeArgumentForImportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupIssueType("issueTypeName")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("issueTypeName", mockConfiguration, mockReferenceLookup);
	}
}
