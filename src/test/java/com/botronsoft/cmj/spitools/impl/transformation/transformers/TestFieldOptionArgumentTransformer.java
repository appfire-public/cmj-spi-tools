package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;

@RunWith(MockitoJUnitRunner.class)
public class TestFieldOptionArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	private OptionsManager mockConfigurationsManager;

	@Mock
	private Option mockFieldOption;

	private FieldOptionArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new FieldOptionArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformFieldArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getOptionsManager()).thenReturn(mockConfigurationsManager);
		when(mockConfigurationsManager.findByOptionId(5L)).thenReturn(mockFieldOption);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectFieldOption("5", mockFieldOption);
		assertThat(transformedValue, is("5"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void transformFieldArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getOptionsManager()).thenReturn(mockConfigurationsManager);
		when(mockConfigurationsManager.findByOptionId(5L)).thenReturn(mockFieldOption);

		String transformedValue = transformer.transformArgumentForExport("five", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectFieldOption("five", mockFieldOption);
		assertThat(transformedValue, is("five"));
	}

	@Test
	public void transformFieldArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getOptionsManager()).thenReturn(mockConfigurationsManager);
		when(mockConfigurationsManager.findByOptionId(5L)).thenReturn(mockFieldOption);

		transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformFieldArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupFieldOption("5")).thenReturn(Optional.of(mockFieldOption));
		when(mockFieldOption.getOptionId()).thenReturn(6L);

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}
}
