package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.issue.link.IssueLinkType;
import com.atlassian.jira.issue.link.IssueLinkTypeManager;

@RunWith(MockitoJUnitRunner.class)
public class TestIssueLinkTypeArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	private IssueLinkTypeManager mockIssueLinkTypeManager;

	@Mock
	private IssueLinkType mockIssueLinkType;

	private IssueLinkTypeArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new IssueLinkTypeArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformIssueLinkTypeArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getIssueLinkTypeManager()).thenReturn(mockIssueLinkTypeManager);
		when(mockIssueLinkTypeManager.getIssueLinkType(5l)).thenReturn(mockIssueLinkType);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectIssueLinkType("5", mockIssueLinkType);
		assertThat(transformedValue, is("5"));
	}

	@Test
	public void transformIssueLinkTypeArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getIssueLinkTypeManager()).thenReturn(mockIssueLinkTypeManager);
		when(mockIssueLinkTypeManager.getIssueLinkTypesByName("issueLinkTypeName"))
				.thenReturn(Collections.singletonList(mockIssueLinkType));

		String transformedValue = transformer.transformArgumentForExport("issueLinkTypeName", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectIssueLinkType("issueLinkTypeName", mockIssueLinkType);
		assertThat(transformedValue, is("issueLinkTypeName"));
	}

	@Test
	public void transformIssueLinkTypeArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getIssueLinkTypeManager()).thenReturn(mockIssueLinkTypeManager);
		when(mockIssueLinkTypeManager.getIssueLinkType(5l)).thenReturn(null);

		transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformIssueLinkTypeArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupIssueLinkType("5")).thenReturn(Optional.of(mockIssueLinkType));
		when(mockIssueLinkType.getId()).thenReturn(6l);

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}

	@Test
	public void transformIssueLinkTypeArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupIssueLinkType("issueLinkTypeName")).thenReturn(Optional.of(mockIssueLinkType));
		when(mockIssueLinkType.getName()).thenReturn("issueLinkTypeName2");

		String transformedValue = transformer.transformArgumentForImport("issueLinkTypeName", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("issueLinkTypeName2"));
	}

	@Test
	public void transformIssueLinkTypeArgumentForImportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupIssueLinkType("issueLinkTypeName")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("issueLinkTypeName", mockConfiguration, mockReferenceLookup);
	}
}
