package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.config.StatusManager;
import com.atlassian.jira.issue.status.Status;

@RunWith(MockitoJUnitRunner.class)
public class TestStatusArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	StatusManager mockStatusManager;

	@Mock
	private Status mockStatus;

	private StatusArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new StatusArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformStatusArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getStatusManager()).thenReturn(mockStatusManager);
		when(mockStatusManager.getStatus("10005")).thenReturn(mockStatus);

		String transformedValue = transformer.transformArgumentForExport("10005", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectStatus("10005", mockStatus);
		assertThat(transformedValue, is("10005"));
	}

	@Test
	public void transformStatusArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getStatusManager()).thenReturn(mockStatusManager);
		when(mockStatusManager.getStatuses()).thenReturn(Collections.singletonList(mockStatus));
		when(mockStatus.getName()).thenReturn("Done");

		String transformedValue = transformer.transformArgumentForExport("Done", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectStatus("Done", mockStatus);
		assertThat(transformedValue, is("Done"));
	}

	@Test
	public void transformStatusArgumentForExportWithNullObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getStatusManager()).thenReturn(mockStatusManager);
		when(mockStatusManager.getStatus("10005")).thenReturn(null);

		transformer.transformArgumentForExport("10005", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformStatusArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupStatus("10005")).thenReturn(Optional.of(mockStatus));
		when(mockStatus.getId()).thenReturn("10006");

		String transformedValue = transformer.transformArgumentForImport("10005", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("10006"));
	}

	@Test
	public void transformStatusArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupStatus("Done")).thenReturn(Optional.of(mockStatus));
		when(mockStatus.getName()).thenReturn("done2");

		String transformedValue = transformer.transformArgumentForImport("Done", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("done2"));
	}

	@Test
	public void transformStatusArgumentForImportWithNullObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupStatus("10005")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("10005", mockConfiguration, mockReferenceLookup);
	}
}
