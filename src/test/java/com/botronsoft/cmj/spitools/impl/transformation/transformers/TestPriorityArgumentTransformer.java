package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.config.PriorityManager;
import com.atlassian.jira.issue.priority.Priority;

@RunWith(MockitoJUnitRunner.class)
public class TestPriorityArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	PriorityManager mockPriorityManager;

	@Mock
	private Priority mockPriority;

	private PriorityArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new PriorityArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformPriorityArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getPriorityManager()).thenReturn(mockPriorityManager);
		when(mockPriorityManager.getPriority("5")).thenReturn(mockPriority);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectPriority("5", mockPriority);
		assertThat(transformedValue, is("5"));
	}

	@Test
	public void transformPriorityArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getPriorityManager()).thenReturn(mockPriorityManager);
		when(mockPriorityManager.getPriorities()).thenReturn(Collections.singletonList(mockPriority));
		when(mockPriority.getName()).thenReturn("Major");

		String transformedValue = transformer.transformArgumentForExport("Major", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectPriority("Major", mockPriority);
		assertThat(transformedValue, is("Major"));
	}

	@Test
	public void transformPriorityArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getPriorityManager()).thenReturn(mockPriorityManager);
		when(mockPriorityManager.getPriority("5")).thenReturn(null);

		transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformPriorityArgumentForExportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getPriorityManager()).thenReturn(mockPriorityManager);
		when(mockPriorityManager.getPriorities()).thenReturn(Collections.emptyList());

		transformer.transformArgumentForExport("Major", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformPriorityArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupPriority("5")).thenReturn(Optional.of(mockPriority));
		when(mockPriority.getId()).thenReturn("6");

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}

	@Test
	public void transformPriorityArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupPriority("Major")).thenReturn(Optional.of(mockPriority));
		when(mockPriority.getName()).thenReturn("Major2");

		String transformedValue = transformer.transformArgumentForImport("Major", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("Major2"));
	}

	@Test
	public void transformPriorityArgumentForImportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupPriority("Major")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("Major", mockConfiguration, mockReferenceLookup);
	}
}
