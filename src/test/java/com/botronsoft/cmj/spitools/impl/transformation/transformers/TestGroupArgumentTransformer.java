package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.security.groups.GroupManager;

@RunWith(MockitoJUnitRunner.class)
public class TestGroupArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	private GroupManager mockGroupManager;

	@Mock
	private Group mockGroup;

	private GroupArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new GroupArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformGroupArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getGroupManager()).thenReturn(mockGroupManager);
		when(mockGroupManager.getGroup("groupName")).thenReturn(mockGroup);

		String transformedValue = transformer.transformArgumentForExport("groupName", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectGroup("groupName", mockGroup);
		assertThat(transformedValue, is("groupName"));
	}

	@Test
	public void transformGroupArgumentForExportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getGroupManager()).thenReturn(mockGroupManager);
		when(mockGroupManager.getGroup("groupName")).thenReturn(null);

		transformer.transformArgumentForExport("groupName", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformGroupArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupGroup("groupName")).thenReturn(Optional.of(mockGroup));
		when(mockGroup.getName()).thenReturn("groupName2");

		String transformedValue = transformer.transformArgumentForImport("groupName", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("groupName2"));
	}

	@Test
	public void transformGroupArgumentForImportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupGroup("groupName")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("groupName", mockConfiguration, mockReferenceLookup);
	}

}
