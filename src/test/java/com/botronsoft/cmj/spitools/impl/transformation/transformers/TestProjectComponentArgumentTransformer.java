package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.bc.project.component.ProjectComponentManager;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectComponentArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	ProjectComponentManager projectComponentManager;

	@Mock
	private ProjectComponent projectComponent;

	private ProjectComponentArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new ProjectComponentArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformIssueTypeArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getProjectComponentManager()).thenReturn(projectComponentManager);
		when(projectComponentManager.getProjectComponent(5l)).thenReturn(projectComponent);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectProjectComponent("5", projectComponent);
		assertThat(transformedValue, is("5"));
	}

	@Test
	public void transformIssueTypeArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getProjectComponentManager()).thenReturn(projectComponentManager);
		when(projectComponentManager.findByComponentNameCaseInSensitive("projectComponentName"))
				.thenReturn(Collections.singletonList(projectComponent));
		when(projectComponent.getName()).thenReturn("projectComponentName");

		String transformedValue = transformer.transformArgumentForExport("projectComponentName", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectProjectComponent("projectComponentName", projectComponent);
		assertThat(transformedValue, is("projectComponentName"));
	}

	@Test
	public void transformIssueTypeArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getProjectComponentManager()).thenReturn(projectComponentManager);
		when(projectComponentManager.getProjectComponent(5l)).thenReturn(null);

		transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformIssueTypeArgumentForExportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getProjectComponentManager()).thenReturn(projectComponentManager);
		when(projectComponentManager.findByComponentNameCaseInSensitive("projectComponentName")).thenReturn(Collections.emptyList());
		when(projectComponent.getName()).thenReturn("projectComponentName");

		transformer.transformArgumentForExport("projectComponentName", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformIssueTypeArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupProjectComponent("5")).thenReturn(Optional.of(projectComponent));
		when(projectComponent.getId()).thenReturn(6l);

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}

	@Test
	public void transformIssueTypeArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupProjectComponent("projectComponentName")).thenReturn(Optional.of(projectComponent));
		when(projectComponent.getName()).thenReturn("projectComponentName2");

		String transformedValue = transformer.transformArgumentForImport("projectComponentName", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("projectComponentName2"));
	}

	@Test
	public void transformIssueTypeArgumentForImportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupProjectComponent("projectComponentName")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("projectComponentName", mockConfiguration, mockReferenceLookup);
	}
}
