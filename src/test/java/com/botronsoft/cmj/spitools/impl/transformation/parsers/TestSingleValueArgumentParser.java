package com.botronsoft.cmj.spitools.impl.transformation.parsers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;
import com.google.common.collect.Sets;

@RunWith(MockitoJUnitRunner.class)
public class TestSingleValueArgumentParser {

	@Mock
	private Configuration mockConfiguration;

	private SingleValueArgumentParser parser;

	@Before
	public void setUp() {
		parser = new SingleValueArgumentParser();
	}

	@Test
	public void parseEmtpyValue() {
		when(mockConfiguration.getLiteralValues()).thenReturn(Sets.newHashSet("none", "any"));

		String value = "";
		Optional<String> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(false));
	}

	@Test
	public void parseValueInLiteralValues() {
		when(mockConfiguration.getLiteralValues()).thenReturn(Sets.newHashSet("none", "any"));

		String value = "none";
		Optional<String> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(false));

		value = "any";
		parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(false));
	}

	@Test
	public void parseValue() {
		when(mockConfiguration.getLiteralValues()).thenReturn(Sets.newHashSet("none", "any"));

		String value = "fieldId";
		Optional<String> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(true));
		assertThat(parsedValue.get(), is(value));
	}

	@Test
	public void parseNullValue() {
		when(mockConfiguration.getLiteralValues()).thenReturn(Sets.newHashSet("none", "all", "any"));

		String value = null;
		Optional<String> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(false));
	}

	@Test
	public void parseValueWithPrefix() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("prefix-");
		String value = "prefix-field1";
		Optional<String> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(true));
		assertThat(parsedValue.get(), is("field1"));
	}

	@Test
	public void parseEmtpyValueWithPrefix() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("prefix-");
		String value = "";
		Optional<String> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(false));
	}

	@Test
	public void parseValueInLiteralValuesWithPrefix() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("prefix-");
		when(mockConfiguration.getLiteralValues()).thenReturn(Sets.newHashSet("none", "any"));

		String value = "none";
		Optional<String> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(false));

		value = "any";
		parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(false));
	}
}
