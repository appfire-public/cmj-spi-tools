package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;

@RunWith(MockitoJUnitRunner.class)
public class TestUserArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	UserManager mockUserManager;

	@Mock
	private ApplicationUser mockUser;

	private UserArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new UserArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformUserArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getUserManager()).thenReturn(mockUserManager);
		when(mockUserManager.getUserByKey("ivancho")).thenReturn(mockUser);

		String transformedValue = transformer.transformArgumentForExport("ivancho", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectUser("ivancho", mockUser);
		assertThat(transformedValue, is("ivancho"));
	}

	@Test
	public void transformUserArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getUserManager()).thenReturn(mockUserManager);
		when(mockUserManager.getUserByName("Ivan")).thenReturn(mockUser);

		String transformedValue = transformer.transformArgumentForExport("Ivan", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectUser("Ivan", mockUser);
		assertThat(transformedValue, is("Ivan"));
	}

	@Test
	public void transformUserArgumentForExportByKey() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_KEY);
		when(mockJiraManagersService.getUserManager()).thenReturn(mockUserManager);
		when(mockUserManager.getUserByKey("ivancho")).thenReturn(mockUser);

		String transformedValue = transformer.transformArgumentForExport("ivancho", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectUser("ivancho", mockUser);
		assertThat(transformedValue, is("ivancho"));
	}

	@Test
	public void transformUserArgumentForExportWithNullObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getUserManager()).thenReturn(mockUserManager);
		when(mockUserManager.getUserByKey("ivancho")).thenReturn(null);

		transformer.transformArgumentForExport("ivancho", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformUserArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupUser("ivancho")).thenReturn(Optional.of(mockUser));
		when(mockUser.getKey()).thenReturn("ivancho2");

		String transformedValue = transformer.transformArgumentForImport("ivancho", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("ivancho2"));
	}

	@Test
	public void transformUserArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupUser("Ivan")).thenReturn(Optional.of(mockUser));
		when(mockUser.getName()).thenReturn("Ivan2");

		String transformedValue = transformer.transformArgumentForImport("Ivan", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("Ivan2"));
	}

	@Test
	public void transformUserArgumentForImportByKey() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_KEY);
		when(mockReferenceLookup.lookupUser("ivancho")).thenReturn(Optional.of(mockUser));
		when(mockUser.getKey()).thenReturn("ivancho2");

		String transformedValue = transformer.transformArgumentForImport("ivancho", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("ivancho2"));
	}

	@Test
	public void transformUserArgumentForImportWithNullObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupUser("ivancho")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("ivancho", mockConfiguration, mockReferenceLookup);
	}
}
