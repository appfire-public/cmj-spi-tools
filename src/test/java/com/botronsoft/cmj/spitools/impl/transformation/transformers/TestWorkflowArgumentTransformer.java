package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestWorkflowArgumentTransformer extends BaseArgumentTransformerTest{

    @Mock
    WorkflowManager mockWorkflowManager;

    @Mock
    private JiraWorkflow mockJiraWorkflow;

    private WorkflowArgumentTransformer transformer;

    @Before
    public void setUp() {
        transformer = new WorkflowArgumentTransformer(mockJiraManagersService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void transformWorkflowArgumentForExportById() {
        when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
        when(mockJiraManagersService.getWorkflowManager()).thenReturn(mockWorkflowManager);
        when(mockWorkflowManager.getWorkflow("5")).thenReturn(mockJiraWorkflow);

        String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

        verify(mockReferenceCollector, times(1)).collectJiraWorkflow("5", mockJiraWorkflow);
        assertThat(transformedValue, is("5"));
    }

    @Test
    public void transformWorkflowArgumentForExportByName() {
        when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
        when(mockJiraManagersService.getWorkflowManager()).thenReturn(mockWorkflowManager);
        when(mockWorkflowManager.getWorkflows()).thenReturn(Collections.singletonList(mockJiraWorkflow));
        when(mockJiraWorkflow.getName()).thenReturn("STORY_WORKFLOW");

        String transformedValue = transformer.transformArgumentForExport("STORY_WORKFLOW", mockConfiguration, mockReferenceCollector);

        verify(mockReferenceCollector, times(1)).collectJiraWorkflow("STORY_WORKFLOW", mockJiraWorkflow);
        assertThat(transformedValue, is("STORY_WORKFLOW"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void transformWorkflowArgumentForExportByIdWithoutObject() {
        when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
        when(mockJiraManagersService.getWorkflowManager()).thenReturn(mockWorkflowManager);
        when(mockWorkflowManager.getWorkflow("5")).thenReturn(mockJiraWorkflow);

        transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
    }

    @Test
    public void transformWorkflowArgumentForExportByNameWithoutObject() {
        when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
        when(mockJiraManagersService.getWorkflowManager()).thenReturn(mockWorkflowManager);
        when(mockWorkflowManager.getWorkflows()).thenReturn(Collections.emptyList());

        transformer.transformArgumentForExport("STORY_WORKFLOW", mockConfiguration, mockReferenceCollector);
    }

    @Test
    public void transformWorkflowArgumentForImportByName() {
        when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
        when(mockReferenceLookup.lookupJiraWorkflow("STORY_WORKFLOW")).thenReturn(Optional.of(mockJiraWorkflow));
        when(mockJiraWorkflow.getName()).thenReturn("STORY_WORKFLOW2");

        String transformedValue = transformer.transformArgumentForImport("STORY_WORKFLOW", mockConfiguration, mockReferenceLookup);
        assertThat(transformedValue, is("STORY_WORKFLOW2"));
    }

    @Test
    public void transformWorkflowArgumentForImportByNameWithoutObject() {
        when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
        when(mockReferenceLookup.lookupJiraWorkflow("STORY_WORKFLOW")).thenReturn(Optional.empty());

        transformer.transformArgumentForImport("STORY_WORKFLOW", mockConfiguration, mockReferenceLookup);
    }
}
