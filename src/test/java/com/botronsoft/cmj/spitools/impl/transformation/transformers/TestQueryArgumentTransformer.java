package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.jql.parser.JqlQueryParser;
import com.atlassian.jira.jql.util.JqlStringSupport;
import com.atlassian.query.Query;

@RunWith(MockitoJUnitRunner.class)
public class TestQueryArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	JqlQueryParser mockQueryParser;

	@Mock
	JqlStringSupport jqlStringSupport;

	@Mock
	private Query mockQuery;
	private QueryArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new QueryArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformQueryArgumentForExport() throws JqlParseException {
		when(mockJiraManagersService.getJqlQueryParser()).thenReturn(mockQueryParser);
		when(mockQueryParser.parseQuery("project = BAN or project = MKY")).thenReturn(mockQuery);

		String transformedValue = transformer.transformArgumentForExport("project = BAN or project = MKY", mockConfiguration,
				mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectQuery("project = BAN or project = MKY", mockQuery);
		assertThat(transformedValue, is("project = BAN or project = MKY"));
	}

	@Test
	public void transformQueryArgumentForImport() {
		when(mockJiraManagersService.getJqlStringSupport()).thenReturn(jqlStringSupport);
		when(jqlStringSupport.generateJqlString(mockQuery)).thenReturn("project = BAN or project = MKY");
		when(mockReferenceLookup.lookupQuery("project = BAN or project = MKY")).thenReturn(Optional.of(mockQuery));

		String transformedValue = transformer.transformArgumentForImport("project = BAN or project = MKY", mockConfiguration,
				mockReferenceLookup);
		assertThat(transformedValue, is("project = BAN or project = MKY"));
	}

	@Test
	public void transformQueryArgumentForImportWithNullObject() {
		when(mockReferenceLookup.lookupQuery("project = BAN or project = MKY")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("project = BAN or project = MKY", mockConfiguration, mockReferenceLookup);
	}
}
