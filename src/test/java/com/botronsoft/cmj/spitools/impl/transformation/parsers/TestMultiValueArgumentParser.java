package com.botronsoft.cmj.spitools.impl.transformation.parsers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;
import com.google.common.collect.Sets;

@RunWith(MockitoJUnitRunner.class)
public class TestMultiValueArgumentParser {

	@Mock
	private Configuration mockConfiguration;

	private MultiValueArgumentParser parser;

	@Before
	public void setup() {
		parser = new MultiValueArgumentParser();

		when(mockConfiguration.getLiteralValues()).thenReturn(Sets.newHashSet("none", "any"));
		when(mockConfiguration.getSeparator()).thenReturn(Optional.of(","));
	}

	@Test
	public void parseEmtpyValue() {
		String value = "";
		Optional<List<String>> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(true));
		assertThat(parsedValue.get().size(), is(0));
	}

	@Test
	public void parseValueInLiteralValues() {
		String value = "none";
		Optional<List<String>> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(false));

		value = "any";
		parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(false));
	}

	@Test
	public void parseQuotedValues() {
		String value = "\"field1\", 'field2', f:\"3\"";
		Optional<List<String>> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(true));
		assertThat(parsedValue.get().size(), is(3));
		assertThat(parsedValue.get().get(0), is("field1"));
		assertThat(parsedValue.get().get(1), is("'field2'"));
		assertThat(parsedValue.get().get(2), is("f:3"));
	}

	@Test
	public void parseValues() {
		String value = "field1, field2, field3";
		Optional<List<String>> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(true));
		assertThat(parsedValue.get().size(), is(3));
		assertThat(parsedValue.get().get(0), is("field1"));
		assertThat(parsedValue.get().get(1), is("field2"));
		assertThat(parsedValue.get().get(2), is("field3"));
	}

	@Test
	public void parseNullValue() {
		String value = null;
		Optional<List<String>> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(false));
	}

	@Test
	public void parseValuesWithPrefix() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("prefix-");
		String value = "prefix-field1, prefix-field2, prefix-field3";
		Optional<List<String>> parsedValue = parser.parse(value, mockConfiguration);
		assertThat(parsedValue.isPresent(), is(true));
		assertThat(parsedValue.get().size(), is(3));
		assertThat(parsedValue.get().get(0), is("field1"));
		assertThat(parsedValue.get().get(1), is("field2"));
		assertThat(parsedValue.get().get(2), is("field3"));
	}
}
