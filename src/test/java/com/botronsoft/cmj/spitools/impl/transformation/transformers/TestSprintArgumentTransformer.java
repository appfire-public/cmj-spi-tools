package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSprintArgumentTransformer extends BaseArgumentTransformerTest{

    private SprintArgumentTransformer transformer;

    @Before
    public void setUp() {
        transformer = new SprintArgumentTransformer(mockJiraManagersService);
    }

    @Test
    public void transformSprintArgumentForExportById() {
        when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);

        String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

        verify(mockReferenceCollector, times(1)).collectSprint("5", 5L);
        assertThat(transformedValue, is("5"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void transformSprintArgumentForExportByName() {
        when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);

        transformer.transformArgumentForExport("sprintName", mockConfiguration, mockReferenceCollector);
    }

    @Test
    public void transformSprintArgumentForImportById() {
        when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
        when(mockReferenceLookup.lookupSprint("5")).thenReturn(Optional.of(6L));

        String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
        assertThat(transformedValue, is("6"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void transformSprintArgumentForImportByName() {
        when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
        when(mockReferenceLookup.lookupSprint("sprintName")).thenReturn(Optional.of(5L));

        transformer.transformArgumentForImport("sprintName", mockConfiguration, mockReferenceLookup);
    }
}
