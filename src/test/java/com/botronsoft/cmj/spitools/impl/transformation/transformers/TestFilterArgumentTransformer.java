package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.search.SearchRequestManager;

@RunWith(MockitoJUnitRunner.class)
public class TestFilterArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	private SearchRequestManager mockSearchRequestManager;

	@Mock
	private SearchRequest mockFilter;

	private FilterArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new FilterArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformFilterArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getSearchRequestManager()).thenReturn(mockSearchRequestManager);
		when(mockSearchRequestManager.getSearchRequestById(5L)).thenReturn(mockFilter);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectSearchRequest("5", mockFilter);
		assertThat(transformedValue, is("5"));
	}

	@Test
	public void transformFilterArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getSearchRequestManager()).thenReturn(mockSearchRequestManager);
		when(mockSearchRequestManager.findByNameIgnoreCase("filterName")).thenReturn(Collections.singletonList(mockFilter));

		String transformedValue = transformer.transformArgumentForExport("filterName", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectSearchRequest("filterName", mockFilter);
		assertThat(transformedValue, is("filterName"));
	}

	@Test
	public void transformFilterArgumentForExportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getSearchRequestManager()).thenReturn(mockSearchRequestManager);
		when(mockSearchRequestManager.findByNameIgnoreCase("filterName")).thenReturn(Collections.emptyList());

		transformer.transformArgumentForExport("filterName", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformFilterArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getSearchRequestManager()).thenReturn(mockSearchRequestManager);
		when(mockReferenceLookup.lookupSearchRequest("5")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
	}

	@Test
	public void transformFilterArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupSearchRequest("5")).thenReturn(Optional.of(mockFilter));
		when(mockFilter.getId()).thenReturn(6L);

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}

	@Test
	public void transformFilterArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupSearchRequest("filterName")).thenReturn(Optional.of(mockFilter));
		when(mockFilter.getName()).thenReturn("filterName2");

		String transformedValue = transformer.transformArgumentForImport("filterName", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("filterName2"));
	}

	@Test
	public void transformFilterArgumentForImportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupSearchRequest("filterName")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("filterName", mockConfiguration, mockReferenceLookup);
	}
}
