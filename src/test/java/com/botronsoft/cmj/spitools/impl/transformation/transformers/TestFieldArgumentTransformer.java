package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;

@RunWith(MockitoJUnitRunner.class)
public class TestFieldArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	private CustomFieldManager mockCustomFieldManager;

	@Mock
	private CustomField mockCustomField;

	private FieldArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new FieldArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformFieldArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getCustomFieldManager()).thenReturn(mockCustomFieldManager);
		when(mockCustomFieldManager.getCustomFieldObject("5")).thenReturn(mockCustomField);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectField("5", mockCustomField);
		assertThat(transformedValue, is("5"));
	}

	@Test
	public void transformFieldArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getCustomFieldManager()).thenReturn(mockCustomFieldManager);
		when(mockCustomFieldManager.getCustomFieldObjectByName("fieldName")).thenReturn(mockCustomField);

		String transformedValue = transformer.transformArgumentForExport("fieldName", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectField("fieldName", mockCustomField);
		assertThat(transformedValue, is("fieldName"));
	}

	@Test
	public void transformFieldArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getCustomFieldManager()).thenReturn(mockCustomFieldManager);
		when(mockCustomFieldManager.getCustomFieldObject("5")).thenReturn(null);

		transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformFieldArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupField("5")).thenReturn(Optional.of(mockCustomField));
		when(mockCustomField.getId()).thenReturn("6");

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}

	@Test
	public void transformFieldArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupField("fieldName")).thenReturn(Optional.of(mockCustomField));
		when(mockCustomField.getName()).thenReturn("fieldName");

		String transformedValue = transformer.transformArgumentForImport("fieldName", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("fieldName"));
	}

	@Test
	public void transformFieldArgumentForImportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupField("fieldName")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("fieldName", mockConfiguration, mockReferenceLookup);
	}
}
