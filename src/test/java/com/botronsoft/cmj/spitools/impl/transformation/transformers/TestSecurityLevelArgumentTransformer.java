package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.issue.security.IssueSecurityLevel;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;

@RunWith(MockitoJUnitRunner.class)
public class TestSecurityLevelArgumentTransformer extends BaseArgumentTransformerTest {
	@Mock
	IssueSecurityLevelManager mockSecurityManager;

	@Mock
	private IssueSecurityLevel mockSecurityLevel;

	private SecurityLevelArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new SecurityLevelArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformSecurityLevelArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getIssueSecurityLevelManager()).thenReturn(mockSecurityManager);
		when(mockSecurityManager.getSecurityLevel(5)).thenReturn(mockSecurityLevel);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectSecurityLevel("5", mockSecurityLevel);
		assertThat(transformedValue, is("5"));
	}

	@Test
	public void transformSecurityLevelArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getIssueSecurityLevelManager()).thenReturn(mockSecurityManager);
		when(mockSecurityManager.getIssueSecurityLevelsByName("securityLevelName"))
				.thenReturn(Collections.singletonList(mockSecurityLevel));
		when(mockSecurityLevel.getName()).thenReturn("securityLevelName");

		String transformedValue = transformer.transformArgumentForExport("securityLevelName", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectSecurityLevel("securityLevelName", mockSecurityLevel);
		assertThat(transformedValue, is("securityLevelName"));
	}

	@Test
	public void transformSecurityLevelArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getIssueSecurityLevelManager()).thenReturn(mockSecurityManager);
		when(mockSecurityManager.getSecurityLevel(5)).thenReturn(null);

		transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformSecurityLevelArgumentForExportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getIssueSecurityLevelManager()).thenReturn(mockSecurityManager);
		when(mockSecurityManager.getIssueSecurityLevelsByName("empty")).thenReturn(Collections.emptyList());

		transformer.transformArgumentForExport("empty", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformSecurityLevelArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupSecurityLevel("5")).thenReturn(Optional.of(mockSecurityLevel));
		when(mockSecurityLevel.getId()).thenReturn(6L);

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}

	@Test
	public void transformSecurityLevelArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupSecurityLevel("securityLevelName")).thenReturn(Optional.of(mockSecurityLevel));
		when(mockSecurityLevel.getName()).thenReturn("securityLevelName2");

		String transformedValue = transformer.transformArgumentForImport("securityLevelName", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("securityLevelName2"));
	}

	@Test
	public void transformSecurityLevelArgumentForImportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupSecurityLevel("securityLevelName")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("securityLevelName", mockConfiguration, mockReferenceLookup);
	}
}
