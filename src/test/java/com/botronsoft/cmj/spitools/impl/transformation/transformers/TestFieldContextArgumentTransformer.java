package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;

@RunWith(MockitoJUnitRunner.class)
public class TestFieldContextArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	private FieldConfigSchemeManager mockConfigurationsManager;

	@Mock
	private FieldConfigScheme mockFieldConfigScheme;

	private FieldContextArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new FieldContextArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformFieldArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getFieldConfigSchemeManager()).thenReturn(mockConfigurationsManager);
		when(mockConfigurationsManager.getFieldConfigScheme(5L)).thenReturn(mockFieldConfigScheme);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectFieldContext("5", mockFieldConfigScheme);
		assertThat(transformedValue, is("5"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void transformFieldArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getFieldConfigSchemeManager()).thenReturn(mockConfigurationsManager);
		when(mockConfigurationsManager.getFieldConfigScheme(5L)).thenReturn(mockFieldConfigScheme);

		String transformedValue = transformer.transformArgumentForExport("five", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectFieldContext("five", mockFieldConfigScheme);
		assertThat(transformedValue, is("five"));
	}

	@Test
	public void transformFieldArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getFieldConfigSchemeManager()).thenReturn(mockConfigurationsManager);
		when(mockConfigurationsManager.getFieldConfigScheme(5L)).thenReturn(mockFieldConfigScheme);

		transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformFieldArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupFieldContext("5")).thenReturn(Optional.of(mockFieldConfigScheme));
		when(mockFieldConfigScheme.getId()).thenReturn(6L);

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}
}
