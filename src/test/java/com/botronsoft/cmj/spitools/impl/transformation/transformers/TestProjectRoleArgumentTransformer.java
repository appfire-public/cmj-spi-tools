package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectRoleArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	ProjectRoleManager mockProjectRoleManager;

	@Mock
	private ProjectRole mockProjectRole;

	private ProjectRoleArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new ProjectRoleArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformProjectRoleArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getProjectRoleManager()).thenReturn(mockProjectRoleManager);
		when(mockProjectRoleManager.getProjectRole(10005L)).thenReturn(mockProjectRole);

		String transformedValue = transformer.transformArgumentForExport("10005", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectProjectRole("10005", mockProjectRole);
		assertThat(transformedValue, is("10005"));
	}

	@Test
	public void transformProjectRoleArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getProjectRoleManager()).thenReturn(mockProjectRoleManager);
		when(mockProjectRoleManager.getProjectRole("Admin")).thenReturn(mockProjectRole);

		String transformedValue = transformer.transformArgumentForExport("Admin", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectProjectRole("Admin", mockProjectRole);
		assertThat(transformedValue, is("Admin"));
	}

	@Test
	public void transformProjectRoleArgumentForExportWithNullObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getProjectRoleManager()).thenReturn(mockProjectRoleManager);
		when(mockProjectRoleManager.getProjectRole(10005L)).thenReturn(null);

		transformer.transformArgumentForExport("10005", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformProjectRoleArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupProjectRole("10005")).thenReturn(Optional.of(mockProjectRole));
		when(mockProjectRole.getId()).thenReturn(10006L);

		String transformedValue = transformer.transformArgumentForImport("10005", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("10006"));
	}

	@Test
	public void transformProjectRoleArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupProjectRole("Admin")).thenReturn(Optional.of(mockProjectRole));
		when(mockProjectRole.getName()).thenReturn("admin2");

		String transformedValue = transformer.transformArgumentForImport("Admin", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("admin2"));
	}

	@Test
	public void transformProjectRoleArgumentForImportWithNullObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupProjectRole("10005")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("10005", mockConfiguration, mockReferenceLookup);
	}
}
