package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import org.mockito.Mock;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spitools.impl.JiraManagersServiceImpl;
import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;

public class BaseArgumentTransformerTest {

	@Mock
	protected JiraManagersServiceImpl mockJiraManagersService;

	@Mock
	protected Configuration mockConfiguration;

	@Mock
	protected ConfigurationReferenceCollector mockReferenceCollector;

	@Mock
	protected ConfigurationReferenceLookup mockReferenceLookup;
}
