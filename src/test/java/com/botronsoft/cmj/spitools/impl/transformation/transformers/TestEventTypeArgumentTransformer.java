package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.event.type.EventTypeManager;

@RunWith(MockitoJUnitRunner.class)
public class TestEventTypeArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	EventTypeManager mockEventTypeManager;

	@Mock
	private EventType mockEventType;

	private EventTypeArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new EventTypeArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformEventTypeArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getEventTypeManager()).thenReturn(mockEventTypeManager);
		when(mockEventTypeManager.getEventType(5L)).thenReturn(mockEventType);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectEventType("5", mockEventType);
		assertThat(transformedValue, is("5"));
	}

	@Test
	public void transformEventTypeArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getEventTypeManager()).thenReturn(mockEventTypeManager);
		when(mockEventTypeManager.getEventTypes()).thenReturn(Collections.singletonList(mockEventType));
		when(mockEventType.getName()).thenReturn("ISSUE_CREATED");

		String transformedValue = transformer.transformArgumentForExport("ISSUE_CREATED", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectEventType("ISSUE_CREATED", mockEventType);
		assertThat(transformedValue, is("ISSUE_CREATED"));
	}

	@Test
	public void transformEventTypeArgumentForExportByIdWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getEventTypeManager()).thenReturn(mockEventTypeManager);
		when(mockEventTypeManager.getEventType(5L)).thenReturn(null);

		transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformEventTypeArgumentForExportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getEventTypeManager()).thenReturn(mockEventTypeManager);
		when(mockEventTypeManager.getEventTypes()).thenReturn(Collections.emptyList());

		transformer.transformArgumentForExport("ISSUE_CREATED", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformEventTypeArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupEventType("5")).thenReturn(Optional.of(mockEventType));
		when(mockEventType.getId()).thenReturn(6L);

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}

	@Test
	public void transformEventTypeArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupEventType("ISSUE_CREATED")).thenReturn(Optional.of(mockEventType));
		when(mockEventType.getName()).thenReturn("ISSUE_CREATED2");

		String transformedValue = transformer.transformArgumentForImport("ISSUE_CREATED", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("ISSUE_CREATED2"));
	}

	@Test
	public void transformEventTypeArgumentForImportByNameWithoutObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupEventType("ISSUE_CREATED")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("ISSUE_CREATED", mockConfiguration, mockReferenceLookup);
	}
}
