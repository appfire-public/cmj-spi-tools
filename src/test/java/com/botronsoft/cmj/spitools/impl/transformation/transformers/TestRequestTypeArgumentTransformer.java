package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

@RunWith(MockitoJUnitRunner.class)
public class TestRequestTypeArgumentTransformer extends BaseArgumentTransformerTest {

	private RequestTypeArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new RequestTypeArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformRequestTypeArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);

		String transformedValue = transformer.transformArgumentForExport("5", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectRequestType("5", 5L);
		assertThat(transformedValue, is("5"));
	}

	@Test(expected = UnsupportedOperationException.class)
	public void transformRequestTypeArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);

		transformer.transformArgumentForExport("securityLevelName", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformRequestTypeArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupRequestType("5")).thenReturn(Optional.of(6L));

		String transformedValue = transformer.transformArgumentForImport("5", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("6"));
	}

	@Test(expected = UnsupportedOperationException.class)
	public void transformRequestTypeArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupRequestType("securityLevelName")).thenReturn(Optional.of(5L));

		transformer.transformArgumentForImport("securityLevelName", mockConfiguration, mockReferenceLookup);
	}

}
