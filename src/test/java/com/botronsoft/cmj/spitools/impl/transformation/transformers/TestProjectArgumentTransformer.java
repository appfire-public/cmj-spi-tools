package com.botronsoft.cmj.spitools.impl.transformation.transformers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.ReferenceType;

import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;

@RunWith(MockitoJUnitRunner.class)
public class TestProjectArgumentTransformer extends BaseArgumentTransformerTest {

	@Mock
	ProjectManager mockProjectManager;

	@Mock
	private Project mockProject;

	private ProjectArgumentTransformer transformer;

	@Before
	public void setUp() {
		transformer = new ProjectArgumentTransformer(mockJiraManagersService);
	}

	@Test
	public void transformProjectArgumentForExportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockJiraManagersService.getProjectManager()).thenReturn(mockProjectManager);
		when(mockProjectManager.getProjectObj(10005L)).thenReturn(mockProject);

		String transformedValue = transformer.transformArgumentForExport("10005", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectProject("10005", mockProject);
		assertThat(transformedValue, is("10005"));
	}

	@Test
	public void transformProjectArgumentForExportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockJiraManagersService.getProjectManager()).thenReturn(mockProjectManager);
		when(mockProjectManager.getProjectObjByName("Monkey")).thenReturn(mockProject);

		String transformedValue = transformer.transformArgumentForExport("Monkey", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectProject("Monkey", mockProject);
		assertThat(transformedValue, is("Monkey"));
	}

	@Test
	public void transformProjectArgumentForExportByKey() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_KEY);
		when(mockJiraManagersService.getProjectManager()).thenReturn(mockProjectManager);
		when(mockProjectManager.getProjectObjByKey("MKY")).thenReturn(mockProject);

		String transformedValue = transformer.transformArgumentForExport("MKY", mockConfiguration, mockReferenceCollector);

		verify(mockReferenceCollector, times(1)).collectProject("MKY", mockProject);
		assertThat(transformedValue, is("MKY"));
	}

	@Test
	public void transformProjectArgumentForExportWithNullObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_KEY);
		when(mockJiraManagersService.getProjectManager()).thenReturn(mockProjectManager);
		when(mockProjectManager.getProjectObjByKey("MKY")).thenReturn(null);

		transformer.transformArgumentForExport("MKY", mockConfiguration, mockReferenceCollector);
	}

	@Test
	public void transformProjectArgumentForImportById() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_ID);
		when(mockReferenceLookup.lookupProject("10005")).thenReturn(Optional.of(mockProject));
		when(mockProject.getId()).thenReturn(10006L);

		String transformedValue = transformer.transformArgumentForImport("10005", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("10006"));
	}

	@Test
	public void transformProjectArgumentForImportByName() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupProject("Monkey")).thenReturn(Optional.of(mockProject));
		when(mockProject.getName()).thenReturn("Monkey2");

		String transformedValue = transformer.transformArgumentForImport("Monkey", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("Monkey2"));
	}

	@Test
	public void transformProjectArgumentForImportByKey() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_KEY);
		when(mockReferenceLookup.lookupProject("MKY")).thenReturn(Optional.of(mockProject));
		when(mockProject.getKey()).thenReturn("MKY2");

		String transformedValue = transformer.transformArgumentForImport("MKY", mockConfiguration, mockReferenceLookup);
		assertThat(transformedValue, is("MKY2"));
	}

	@Test
	public void transformProjectArgumentForImportWithNullObject() {
		when(mockConfiguration.getReferenceType()).thenReturn(ReferenceType.BY_NAME);
		when(mockReferenceLookup.lookupProject("Monkey")).thenReturn(Optional.empty());

		transformer.transformArgumentForImport("Monkey", mockConfiguration, mockReferenceLookup);
	}
}
