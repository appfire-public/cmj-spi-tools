package com.botronsoft.cmj.spitools.impl.transformation.producers;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spitools.impl.transformation.configuration.Configuration;

@RunWith(MockitoJUnitRunner.class)
public class TestSingleValueArgumentProducer {

	@Mock
	private Configuration mockConfiguration;

	private SingleValueArgumentProducer producer;

	@Before
	public void setUp() {
		producer = new SingleValueArgumentProducer();
	}

	@Test
	public void produceValueWithPrefix() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("prefix-");
		String value = "field1";
		String parsedValue = producer.produce(value, mockConfiguration);
		assertThat(parsedValue, is("prefix-field1"));
	}

	@Test
	public void produceValueWithEmptyPrefix() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("  ");
		String value = "field1";
		String parsedValue = producer.produce(value, mockConfiguration);
		assertThat(parsedValue, is("field1"));
	}

	@Test
	public void produceEmptyValueWithPrefix() {
		when(mockConfiguration.getReferenceValuePrefix()).thenReturn("prefix-");
		String value = "";
		String parsedValue = producer.produce(value, mockConfiguration);
		assertThat(parsedValue, is("prefix-"));
	}
}
