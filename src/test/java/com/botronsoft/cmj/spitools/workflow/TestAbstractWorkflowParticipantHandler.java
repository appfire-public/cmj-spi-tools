package com.botronsoft.cmj.spitools.workflow;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceCollector;
import com.botronsoft.cmj.spi.configuration.ConfigurationReferenceLookup;
import com.botronsoft.cmj.spi.configuration.ExportContext;
import com.botronsoft.cmj.spi.configuration.ImportContext;
import com.botronsoft.cmj.spi.configuration.workflow.WorkflowParticipantHandler;
import com.botronsoft.cmj.spitools.dsl.ArgumentType;
import com.botronsoft.cmj.spitools.impl.JiraManagersService;
import com.google.common.collect.Lists;

import com.atlassian.jira.config.IssueTypeManager;
import com.atlassian.jira.issue.issuetype.IssueType;

@RunWith(MockitoJUnitRunner.class)
public class TestAbstractWorkflowParticipantHandler {

	@Mock
	private ExportContext mockExportContext;

	@Mock
	private ImportContext mockImportContext;

	@Mock
	private ConfigurationReferenceCollector mockReferenceCollector;

	@Mock
	private ConfigurationReferenceLookup mockReferenceLookup;

	@Mock
	private JiraManagersService mockJiraManagersService;

	@Mock
	private IssueTypeManager mockIssueTypeManager;

	@Mock
	private IssueType mockIssueType1;

	@Mock
	private IssueType mockIssueType2;

	@Before
	public void setUp() {
		when(mockExportContext.getReferenceCollector()).thenReturn(mockReferenceCollector);
		when(mockImportContext.getReferenceLookup()).thenReturn(mockReferenceLookup);
	}

	@Test
	public void exportSingleValueArgument() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10003")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueTypes()).thenReturn(Lists.newArrayList(mockIssueType1, mockIssueType2));
		when(mockIssueType1.getName()).thenReturn("User Story");
		when(mockIssueType2.getName()).thenReturn("Bug");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasSingleValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byName().and().
						hasSingleValueArgument("arg2").ofType(ArgumentType.ISSUE_TYPE).byId();
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "10003");

		Map<String, String> outputMap = handler.transformArgumentsForExport("some.condition", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug"));
		assertThat(outputMap, hasEntry("arg2", "10003"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10003", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("Bug", mockIssueType2);
	}

	@Test
	public void exportSingleValueArgumentWithPrefix() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10003")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueTypes()).thenReturn(Lists.newArrayList(mockIssueType1, mockIssueType2));
		when(mockIssueType1.getName()).thenReturn("User Story");
		when(mockIssueType2.getName()).thenReturn("Bug");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasSingleValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byName().withPrefix("prefixName-").and().
						hasSingleValueArgument("arg2").ofType(ArgumentType.ISSUE_TYPE).byId().withPrefix("prefixId-");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "prefixName-Bug");
		inputMap.put("arg2", "prefixId-10003");

		Map<String, String> outputMap = handler.transformArgumentsForExport("some.condition", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug"));
		assertThat(outputMap, hasEntry("arg2", "10003"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10003", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("Bug", mockIssueType2);
	}

	@Test
	public void exportSingleValueArgumentWithNullableOrEmptyPrefix() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10003")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueTypes()).thenReturn(Lists.newArrayList(mockIssueType1, mockIssueType2));
		when(mockIssueType1.getName()).thenReturn("User Story");
		when(mockIssueType2.getName()).thenReturn("Bug");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasSingleValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byName().withPrefix(null).and().
						hasSingleValueArgument("arg2").ofType(ArgumentType.ISSUE_TYPE).byId().withPrefix("  ");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "10003");

		Map<String, String> outputMap = handler.transformArgumentsForExport("some.condition", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug"));
		assertThat(outputMap, hasEntry("arg2", "10003"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10003", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("Bug", mockIssueType2);
	}

	@Test
	public void exportMultiValueArgument() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10005")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueType("10001")).thenReturn(mockIssueType2);

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				validator("some.validator").
						hasMultiValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byId().withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformArgumentsForExport("some.validator", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10005,10001"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10005", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("10001", mockIssueType2);
	}

	@Test
	public void exportMultiValueArgumentWithPrefix() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10005")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueType("10001")).thenReturn(mockIssueType2);

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				validator("some.validator").
						hasMultiValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byId().withPrefix("prefixId-").withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "prefixId-10005,prefixId-10001");

		Map<String, String> outputMap = handler.transformArgumentsForExport("some.validator", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10005,10001"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10005", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("10001", mockIssueType2);
	}

	@Test
	public void exportMultiValueArgumentWithEmptyPrefix() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10005")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueType("10001")).thenReturn(mockIssueType2);

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				validator("some.validator").
						hasMultiValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byId().withPrefix("   ").withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformArgumentsForExport("some.validator", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10005,10001"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10005", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("10001", mockIssueType2);
	}

	@Test
	public void exportMultiValueArgumentWithNullablePrefix() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10005")).thenReturn(mockIssueType1);
		when(mockIssueTypeManager.getIssueType("10001")).thenReturn(mockIssueType2);

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				validator("some.validator").
						hasMultiValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byId().withPrefix(null).withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformArgumentsForExport("some.validator", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10005,10001"));

		verify(mockReferenceCollector, times(1)).collectIssueType("10005", mockIssueType1);
		verify(mockReferenceCollector, times(1)).collectIssueType("10001", mockIssueType2);
	}

	@Test
	public void exportSingleValueArgumentLiteralValue() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueTypes()).thenReturn(Lists.newArrayList(mockIssueType1, mockIssueType2));
		when(mockIssueType1.getName()).thenReturn("User Story");
		when(mockIssueType2.getName()).thenReturn("Bug");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				postFunction("some.postFunction").
						hasSingleValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byName().and().
						hasSingleValueArgument("arg2").ofType(ArgumentType.ISSUE_TYPE).byId().withLiteralValues("none", "maybe");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "none");

		Map<String, String> outputMap = handler.transformArgumentsForExport("some.postFunction", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug"));
		assertThat(outputMap, hasEntry("arg2", "none"));

		verify(mockReferenceCollector, times(1)).collectIssueType("Bug", mockIssueType2);
	}

	@Test
	public void exportMultiValueArgumentLiteral() {
		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasMultiValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byId().withSeparator(",").withLiteralValues("none");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "none");

		Map<String, String> outputMap = handler.transformArgumentsForExport("some.condition", inputMap, mockExportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "none"));
	}

	@Test
	public void argumentsValidation() {
		when(mockJiraManagersService.getIssueTypeManager()).thenReturn(mockIssueTypeManager);
		when(mockIssueTypeManager.getIssueType("10003")).thenReturn(mockIssueType1);

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasSingleValueArgument("arg1").ofType(ArgumentType.REQUEST_TYPE).byName().and().
						hasSingleValueArgument("arg2").ofType(ArgumentType.ISSUE_TYPE).byId().and().
						hasSingleValueArgument("arg3").ofType(ArgumentType.SPRINT).byName();
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "5");
		inputMap.put("arg2", "10003");
		inputMap.put("arg3", "10001");

		Map<String, String> outputMap = handler.transformArgumentsForExport("some.condition", inputMap, mockExportContext);

		assertThat(outputMap, equalTo(inputMap));

		verify(mockReferenceCollector, times(1)).collectIssueType("10003", mockIssueType1);
		verify(mockReferenceCollector, times(0)).collectRequestType("5", 5L);
		verify(mockReferenceCollector, times(0)).collectSprint("1001", 1001L);
	}

	@Test
	public void importSingleValueArgument() {
		when(mockReferenceLookup.lookupIssueType("Bug")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10003")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getName()).thenReturn("Bug2");
		when(mockIssueType2.getId()).thenReturn("10005");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasSingleValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byName().and().
						hasSingleValueArgument("arg2").ofType(ArgumentType.ISSUE_TYPE).byId();
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "10003");

		Map<String, String> outputMap = handler.transformArgumentsForImport("some.condition", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug2"));
		assertThat(outputMap, hasEntry("arg2", "10005"));
	}

	@Test
	public void importSingleValueArgumentWithPrefix() {
		when(mockReferenceLookup.lookupIssueType("prefixName-Bug")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("prefixId-10003")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getName()).thenReturn("Bug2");
		when(mockIssueType2.getId()).thenReturn("10005");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasSingleValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byName().withPrefix("prefixName-").and().
						hasSingleValueArgument("arg2").ofType(ArgumentType.ISSUE_TYPE).byId().withPrefix("prefixId-");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "prefixName-Bug");
		inputMap.put("arg2", "prefixId-10003");

		Map<String, String> outputMap = handler.transformArgumentsForImport("some.condition", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "prefixName-Bug2"));
		assertThat(outputMap, hasEntry("arg2", "prefixId-10005"));
	}

	@Test
	public void importSingleValueArgumentWithNullableOrEmptyPrefix() {
		when(mockReferenceLookup.lookupIssueType("Bug")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10003")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getName()).thenReturn("Bug2");
		when(mockIssueType2.getId()).thenReturn("10005");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasSingleValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byName().withPrefix("  ").and().
						hasSingleValueArgument("arg2").ofType(ArgumentType.ISSUE_TYPE).byId().withPrefix(null);
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "10003");

		Map<String, String> outputMap = handler.transformArgumentsForImport("some.condition", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug2"));
		assertThat(outputMap, hasEntry("arg2", "10005"));
	}

	@Test
	public void importMultiValueArgument() {
		when(mockReferenceLookup.lookupIssueType("10005")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10001")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getId()).thenReturn("10015");
		when(mockIssueType2.getId()).thenReturn("10011");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasMultiValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byId().withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformArgumentsForImport("some.condition", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10015,10011"));
	}

	@Test
	public void importMultiValueArgumentWithPrefix() {
		when(mockReferenceLookup.lookupIssueType("10005")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10001")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getId()).thenReturn("10015");
		when(mockIssueType2.getId()).thenReturn("10011");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasMultiValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byId().withPrefix("prefixId-").withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "prefixId-10005,prefixId-10001");

		Map<String, String> outputMap = handler.transformArgumentsForImport("some.condition", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "prefixId-10015,prefixId-10011"));
	}

	@Test
	public void importMultiValueArgumentWithNullablePrefix() {
		when(mockReferenceLookup.lookupIssueType("10005")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10001")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getId()).thenReturn("10015");
		when(mockIssueType2.getId()).thenReturn("10011");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasMultiValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byId().withPrefix(null).withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformArgumentsForImport("some.condition", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10015,10011"));
	}

	@Test
	public void importMultiValueArgumentWithEmptyPrefix() {
		when(mockReferenceLookup.lookupIssueType("10005")).thenReturn(Optional.of(mockIssueType1));
		when(mockReferenceLookup.lookupIssueType("10001")).thenReturn(Optional.of(mockIssueType2));

		when(mockIssueType1.getId()).thenReturn("10015");
		when(mockIssueType2.getId()).thenReturn("10011");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasMultiValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byId().withPrefix("  ").withSeparator(",");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "10005,10001");

		Map<String, String> outputMap = handler.transformArgumentsForImport("some.condition", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "10015,10011"));
	}

	@Test
	public void importSingleValueArgumentLiteral() {
		when(mockReferenceLookup.lookupIssueType("Bug")).thenReturn(Optional.of(mockIssueType1));
		when(mockIssueType1.getName()).thenReturn("Bug2");

		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasSingleValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byName().and().
						hasSingleValueArgument("arg2").ofType(ArgumentType.ISSUE_TYPE).byId().withLiteralValues("none");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "Bug");
		inputMap.put("arg2", "none");

		Map<String, String> outputMap = handler.transformArgumentsForImport("some.condition", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(2));
		assertThat(outputMap, hasEntry("arg1", "Bug2"));
		assertThat(outputMap, hasEntry("arg2", "none"));
	}

	@Test
	public void importMultiValueArgumentLiteral() {
		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {

			@Override
			protected void describeArguments() {
				//@formatter:off
				condition("some.condition").
						hasMultiValueArgument("arg1").ofType(ArgumentType.ISSUE_TYPE).byId().withSeparator(",").withLiteralValues("none");
				//@formatter:on
			}
		};

		Map<String, String> inputMap = new HashMap<>();
		inputMap.put("arg1", "none");

		Map<String, String> outputMap = handler.transformArgumentsForImport("some.condition", inputMap, mockImportContext);

		assertThat(outputMap.size(), is(1));
		assertThat(outputMap, hasEntry("arg1", "none"));
	}

	@Test
	public void unhandledArgumentOnExport() {
		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {
			@Override
			protected void describeArguments() {

			}
		};

		Map<String, String> inputMap = Collections.singletonMap("my.prop", "value 1");

		Map<String, String> outputMap = handler.transformArgumentsForExport("participant", inputMap, mockExportContext);
		assertThat(outputMap, equalTo(inputMap));
	}

	@Test
	public void unhandledArgumentOnImport() {
		WorkflowParticipantHandler handler = new ErrorThrowingWorkflowParticipantHandler(mockJiraManagersService) {
			@Override
			protected void describeArguments() {

			}
		};

		Map<String, String> inputMap = Collections.singletonMap("my.prop", "value 1");

		Map<String, String> outputMap = handler.transformArgumentsForImport("participant", inputMap, mockImportContext);
		assertThat(outputMap, equalTo(inputMap));
	}

	private static abstract class ErrorThrowingWorkflowParticipantHandler extends AbstractWorkflowParticipantHandler {

		public ErrorThrowingWorkflowParticipantHandler(JiraManagersService jiraManagersService) {
			super(jiraManagersService);
		}

		@Override
		protected void handleError(String className, String argumentName, Throwable t) {
			throw new IllegalStateException(t);
		}
	}

}
