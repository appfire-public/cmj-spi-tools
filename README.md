# Project Title

Configuration Manager SPI Tools is a helper library, designed to greatly facilitate the implementation of Configuration Manager's SPI. 
It provides a domain-specific language for describing the semantics of argument values in transition participants (conditions, validators, post-functions). 

## Getting Started

Add the SPI Tools as a maven dependency in your pom.xml:

```
<!-- CMJ SPI Tools library -->
<dependency>
   <groupId>com.botronsoft.cmj.spi</groupId>
   <artifactId>configuration-manager-spi-tools</artifactId>
   <version>1.0.0</version>
   <scope>compile</scope>
</dependency>
```

The dependency scope for SPI Tools should be "compile", unlike the dependency scope of the SPI itself which should be "provided".
The SPI Tools library must be bundled with your app.

### Usage

Using the SPI tools library is as simple as extending one of the classes annotated with @PublicApi.
For example if your app provides workflow participants (conditions, validators, post-functions), extend the
com.botronsoft.cmj.spitools.workflow.AbstractWorkflowParticipantHandler class and implement the describeArguments() method
using the SPI Tools DSL, describing the arguments of all transition participants.

```
@Override
protected void describeArguments() {
    condition("org.example.jira.MyConditionClass").
        hasMultiValueArgument("statusIds").ofType(ArgumentType.STATUS)
            .withSeparator(",").withLiteralValues("none").and().
        hasSingleValueArgument("user").byKey().ofType(ArgumentType.USER);
}
```

More information on usage and syntax may be found in the official documentation
of [Configuration Manager for Jira](https://botronsoft.atlassian.net/wiki/spaces/CMJ/pages/622755882/Service+Provider+Interface+SPI) and the javadoc of the [SPI Tools project](http://docs.botronsoft.com).

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/botronsoft/cmj-spi-tools). 

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details

